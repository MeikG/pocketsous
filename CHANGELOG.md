# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.3.1](https://gitlab.com/MeikG/pocketsous/compare/v1.3.0...v1.3.1) (2020-05-14)

**Note:** Version bump only for package pocketsous





# [1.3.0](https://gitlab.com/MeikG/pocketsous/compare/v1.1.0...v1.3.0) (2020-05-13)


### Bug Fixes

* fix CI not producing a build ([ec42070](https://gitlab.com/MeikG/pocketsous/commit/ec42070e197fbb569d55522bc470413b9b1573b8))





# [1.2.0](https://gitlab.com/MeikG/pocketsous/compare/v1.1.0...v1.2.0) (2020-05-13)


### Bug Fixes

* **image_uploads:** update row level security ([d581779](https://gitlab.com/MeikG/pocketsous/commit/d581779050bba1f9823d2dbf16357b4523cd2d31))
* **image-upload:** fix Express routes ([69f450c](https://gitlab.com/MeikG/pocketsous/commit/69f450c21fa753eba9d50cb099b755cacffdff06))
* **recipes:** update row level security ([7d70319](https://gitlab.com/MeikG/pocketsous/commit/7d7031931ba371adffec7579308c39cc3ea27439))
* **search:** fix incorrect autocomplete function volatility ([a3385a0](https://gitlab.com/MeikG/pocketsous/commit/a3385a0f5ebc006d8a26e7547e6ec1e3a0ac44c2))


### Features

* **recipe_scheduling:** add initial recipe scheduling functionality ([718f909](https://gitlab.com/MeikG/pocketsous/commit/718f9099d60242b54b82f0cc14fba1969175fd9f))
* add Apollo ([288647f](https://gitlab.com/MeikG/pocketsous/commit/288647f81eb26322d8bb3848846a8fd7878c96ed))
* add React Native Paper ([dc16205](https://gitlab.com/MeikG/pocketsous/commit/dc162055a3d2f142dad2ac22ed85223974cc8b7e))
* add version endpoint to each of the backend microservices ([9f19ee6](https://gitlab.com/MeikG/pocketsous/commit/9f19ee61027cf8fcfae6808b92427332c8c64eab))





# 1.1.0 (2020-04-14)


### Bug Fixes

* add back expo global install ([0313549](https://gitlab.com/MeikG/pocketsous/commit/03135493a599ac6f8e1af6fd1514a013708aa6c0))
* add ci to deploy stage as Expo needs it ([1a0b2e9](https://gitlab.com/MeikG/pocketsous/commit/1a0b2e99b93ad57320ec73f78931e9294890fc1e))
* add docker pull command to deploy stage ([3c753d5](https://gitlab.com/MeikG/pocketsous/commit/3c753d552e397a8f6f76876c5c9d9d4b6a19247f))
* add expo login ([db50346](https://gitlab.com/MeikG/pocketsous/commit/db503461a10dd4bed760a84795aef257c6d4144a))
* add missing cd to app directory ([48815b1](https://gitlab.com/MeikG/pocketsous/commit/48815b1b884f7757f4747cd630fda37a6b879ec6))
* add quotes around expo login, remove global install and replace with npx ([fca7cf8](https://gitlab.com/MeikG/pocketsous/commit/fca7cf80c5b6f4b5976efee432087cabdb0f60bc))
* change docker to 18, and fix typo in version exclusion ([25f1afe](https://gitlab.com/MeikG/pocketsous/commit/25f1afe7703a2fd093a7b28a24ec8fefd857471a))
* change expo login to non-interactive ([0603a42](https://gitlab.com/MeikG/pocketsous/commit/0603a42adff387466511f1b2d44c8d240fcb8b2e))
* hardcode docker version, and add git dependencies for expo build ([84b154f](https://gitlab.com/MeikG/pocketsous/commit/84b154f767119554b1924503cfb38b6955deb795))
* incorrect stage in ci ([6fb2f41](https://gitlab.com/MeikG/pocketsous/commit/6fb2f41f968ee14df790f8f94a31d9641ac35a38))
* move ci variables to top, update docker version, and fix deploy stage ([7603e71](https://gitlab.com/MeikG/pocketsous/commit/7603e71b3501db0aea34c66db0892a87bc5692de))
* remove changelog and trigger version ([5337a04](https://gitlab.com/MeikG/pocketsous/commit/5337a0449129a3c06d9d5967f2bd13d18c8f8ee7))
* remove platform from expo publish:set ([03daa79](https://gitlab.com/MeikG/pocketsous/commit/03daa791c1171407029e04534eeb22e39dc0f743))
* replace echo test with npm script to run pre-start ([be28da9](https://gitlab.com/MeikG/pocketsous/commit/be28da961e99ddb8e6416b4be110f41effe55f08))
* replace semantic-version with lerna ([d474e19](https://gitlab.com/MeikG/pocketsous/commit/d474e193c8e66ab25e32853b527642f42f0e09f1))
* replace string substitution in lerna with conventional commits ([7c747d4](https://gitlab.com/MeikG/pocketsous/commit/7c747d424e25ee47a13ee42d6ea36919e53dbf7e))
* revert to older docker version ([0cf0e45](https://gitlab.com/MeikG/pocketsous/commit/0cf0e45a2efb0ac1b064a479cb2fde381d28e9a4))
* rewrite docker tags, and add deployment stages ([b86d418](https://gitlab.com/MeikG/pocketsous/commit/b86d41827181f1e60d4d84460e73ad7017d881b8))
* swap git and exec in releaserc, and add all assets to git ([17a3723](https://gitlab.com/MeikG/pocketsous/commit/17a3723b5b8f55703fe5764f6e0c90fc9d496b8b))
* test echoing expo user, and remove npx ([b085dfb](https://gitlab.com/MeikG/pocketsous/commit/b085dfb40352e3427670ee07c903e17bc54b7f05))
* update deployment script to single stage ([5ba8802](https://gitlab.com/MeikG/pocketsous/commit/5ba880244d2630d2fa7ee76888450d18874297c7))
* update expo to use password passed in ([d724534](https://gitlab.com/MeikG/pocketsous/commit/d724534bfe005746b7337725bf96295ba23ad89e))
* update node dependency to current in deploy script ([f26ec93](https://gitlab.com/MeikG/pocketsous/commit/f26ec93dab84a1abdae5d035370dbe01d605b4df))


### Features

* add Dockerfile to app and build as a static website inside an nginx container ([35999bd](https://gitlab.com/MeikG/pocketsous/commit/35999bd1419aee88e1d3eb27b2843ee3155f1a9e))
* add PATH_PREFIX to microservices to allow them to run on dynamic paths ([c82fb04](https://gitlab.com/MeikG/pocketsous/commit/c82fb04ffa57a0868c83f26c86014cd27cf34a0c))
