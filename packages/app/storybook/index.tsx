import React, { Component, ReactNode } from 'react';
import StorybookUIRoot from './storybook';

export default class extends Component {
  public render(): ReactNode {
    return <StorybookUIRoot />;
  }
}
