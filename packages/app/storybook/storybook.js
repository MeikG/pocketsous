import { getStorybookUI, configure } from '@storybook/react-native';
import './rn-addons';
import { loadStories } from './stories';

// import stories
configure(() => {
  loadStories();
}, module);

// Refer to https://github.com/storybookjs/storybook/tree/master/app/react-native#start-command-parameters
// To find allowed options for getStorybookUI
const StorybookUIRoot = getStorybookUI({
  asyncStorage: require('react-native').AsyncStorage,
  onDeviceUI: true,
});

export default StorybookUIRoot;
