module.exports = {
  preset: 'react-native',
  coveragePathIgnorePatterns: ['/node_modules/'],
  rootDir: './src',
  testTimeout: 3600000,
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: '../../',
        classNameTemplate: '{classname}',
        titleTemplate: '{title}',
        uniqueOutputName: 'true',
        ancestorSeparator: ' › ',
      },
    ],
  ],
};
