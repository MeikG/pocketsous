#!/usr/bin/env sh

if [[ -d "/tmp/PROJECT_ROOT" ]]
then
  # Replace all instances of /PROJECT_ROOT with $PATH_PREFIX.
  find /tmp/PROJECT_ROOT -type f -exec awk -v PATH_PREFIX="$PATH_PREFIX" -i inplace '{gsub(/\/PROJECT_ROOT/,PATH_PREFIX)}; { print }' {} \;
  awk -v PATH_PREFIX="$PATH_PREFIX" -i inplace '{gsub(/\/PROJECT_ROOT/,PATH_PREFIX)}; { print }' /home/nginx.conf
  mkdir -p "/var/www${PATH_PREFIX}"
  mv /tmp/PROJECT_ROOT/* /var/www${PATH_PREFIX}

  # Move the nginx config to the correct folder.
  mv /home/nginx.conf /etc/nginx/conf.d/default.conf

  rm -rf /tmp/PROJECT_ROOT
fi
# Start nginx.
exec nginx -g 'daemon off;'
