import React from 'react';
import { render, RenderResult } from '@testing-library/react-native';
import { <%= typeName %> } from './<%= fileName %>.container';
import { <%= typeName %>Fragment } from '../../generated/graphql';

describe('<%= typeName %>', () => {
  it('renders correctly', () => {
    const source: <%= typeName %>Fragment = {
      __typename: 'Query',
    };

    const { container }: RenderResult = render(<<%= typeName %> {...source} />);
    expect(container).toMatchSnapshot();
  });
});
