import React, { PureComponent, ReactNode } from 'react';
import { View } from 'react-native';
import { <%= typeName %>SharedProps } from './<%= fileName %>.container';
import style from './<%= fileName %>.style';

// Component Props.
type Props = <%= typeName %>SharedProps & {
  // @TODO Add Props
};

/**
 * <%= typeName %> Component
 *
 * @TODO Write description
 */
export default class extends PureComponent<Props> {
  public render(): ReactNode {
    const { style: propStyle, children } = this.props;

    return (
      <View style={[style.container, propStyle && propStyle]}>{children}</View>
    );
  }
}
