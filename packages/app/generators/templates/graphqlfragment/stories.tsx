// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { <%= typeName %>Fragment } from '../../generated/graphql';
import { <%= typeName %> } from './<%= fileName %>.container';

const source: <%= typeName %>Fragment = {
  __typename: 'Query',
};

storiesOf('Component/<%= typeName %>', module).add('Basic', () => <<%= typeName %> {...source} />);
