import React, { ComponentClass, PureComponent, ReactNode } from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import { ComponentEnhancer, compose } from 'recompose';
import { <%= typeName %>Fragment } from '../../generated/graphql';
import { PropsWithDefaults } from '../../helpers/typescript.helper';
import <%= typeName %>Component from './<%= fileName %>.component';

// Container Props.
type Props = <%= typeName %>SharedProps & {
  // @TODO Add Props.
};

// Props injected from the HOCs.
type HigherOrderProps = Props & {};

// Props shared with Component.
export type <%= typeName %>SharedProps = <%= typeName %>Fragment & {
  // @TODO Add Shared Props.
  style?: StyleProp<ViewStyle>;
};

type State = {
  // @TODO Add State.
};

/**
 * <%= typeName %> Container.
 *
 * @TODO Write description
 */
class <%= typeName %>Container extends PureComponent<HigherOrderProps, State> {
  // tslint:disable-next-line:typedef
  public static defaultProps = {};

  public readonly state: State = {};

  public render(): ReactNode {
    // const {} = this.state;

    return <<%= typeName %>Component {...this.props} />;
  }
}

// Compose the HOCs.
const composedComponent: ComponentEnhancer<HigherOrderProps, Props> = compose();

export const <%= typeName %>: ComponentClass<
  PropsWithDefaults<Props, typeof <%= typeName %>Container.defaultProps>,
  State
> = composedComponent(<%= typeName %>Container);
