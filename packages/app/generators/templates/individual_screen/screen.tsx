import React, { PureComponent, ReactNode } from 'react';
import { compose, DataProps } from 'react-apollo';
import { View } from 'react-native';
import { NavigationInjectedProps, withNavigation } from 'react-navigation';
import { withMappedNavigationParams } from 'react-navigation-props-mapper';
import { <%= typeName %>Query } from '../../generated/graphql';
import { with<%= typeName %>Query } from './<%= fileName %>.graphql';
// import i18n from '../../language';
import style from './<%= fileName %>.style';

// Screen params for React-Navigation.
export type <%= typeName %>Params = {
  // @TODO Add screen params.
};

// Screen Props.
type Props = <%= typeName %>Params &
  NavigationInjectedProps<<%= typeName %>Params> &
  DataProps<<%= typeName %>Query>;

// Screen State.
type State = {
    // @TODO Add screen state (or remove if unused).
};

/**
 * <%= typeName %> Screen
 *
 * @TODO Write description
 */
class <%= typeName %> extends PureComponent<Props, State> {
  public readonly state: State = {};

  public render(): ReactNode {
    // const {} = this.props;
    // const {} = this.state;

    return <View style={style.container} />;
  }
}

export default compose(
  withNavigation,
  withMappedNavigationParams(),
  with<%= typeName %>Query
)(<%= typeName %>);
