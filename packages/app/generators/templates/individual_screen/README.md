# <%= typeName %>

## Usage

```tsx
import <%= typeName %> from './<%= typeName %>';

<<%= typeName %> />
```
