import { StyleSheet, ViewStyle } from 'react-native';

// tslint:disable-next-line:typedef
const styles = {
  // @TODO Add Styles
  container: {
    flex: 1,
  } as ViewStyle,
};

export default StyleSheet.create(styles);
