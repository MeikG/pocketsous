import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { QueryResult } from 'react-apollo';
import { <%= typeName %>Query, <%= typeName %>QueryVariables } from '../../generated/graphql';
import { <%= typeName %> } from './';
import { MockedProvider, MockedResponse } from 'react-apollo/test-utils';
import <%= upperSnakeCase %>_QUERY from './<%= fileName %>.graphql';

// GraphQL request variables.
const variables: <%= typeName %>QueryVariables = {};

const result: Partial<QueryResult<<%= typeName %>Query>> = {
  data: {
    __typename: 'Query',
    // @TODO Add query result mock.
  },
};

// A complete request for the screen with all its data.
const screen: MockedResponse = {
  request: {
    query: <%= upperSnakeCase %>_QUERY,
    variables,
  },
  result,
};

// A partial response from the server, to mock an incomplete cache-driven response.
const partial: MockedResponse = {
  ...screen,
  result: {
    ...screen.result,
    // Add a partial data structure.
    data: {},
  },
};

// A 5 second loading delay.
const loading: MockedResponse = {
  ...screen,
  delay: 5000,
};

// A mocked generic error response.
const error: MockedResponse = {
  ...screen,
  error: new Error('error'),
};

storiesOf('Screens/<%= typeName %>', module).add('Loading', () => (
  <MockedProvider mocks={[loading]} addTypename={false}>
    <<%= typeName %> />
  </MockedProvider>
));

storiesOf('Screens/<%= typeName %>', module).add('Error', () => (
  <MockedProvider mocks={[error]} addTypename={false}>
    <<%= typeName %> />
  </MockedProvider>
));

storiesOf('Screens/<%= typeName %>', module).add('Partial', () => (
  <MockedProvider mocks={[partial]} addTypename={false}>
    <<%= typeName %> />
  </MockedProvider>
));

storiesOf('Screens/<%= typeName %>', module).add('Screen', () => (
  <MockedProvider mocks={[screen]} addTypename={false}>
    <<%= typeName %> />
  </MockedProvider>
));
