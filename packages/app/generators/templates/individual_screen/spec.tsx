import React from 'react';
import { QueryResult } from 'react-apollo';
import { MockedProvider, MockedResponse } from 'react-apollo/test-utils';
import { render, RenderAPI } from 'react-native-testing-library';
import { <%= typeName %>Query, <%= typeName %>QueryVariables } from '../../generated/graphql';
import { <%= upperSnakeCase %>_QUERY, <%= typeName %> } from './';

// Small utility function to wait for a specified time.
function timeout(time?: number) {
  return new Promise(resolve => setTimeout(resolve, time));
}

describe('<%= typeName %> Screen', () => {
  it('renders correctly', async () => {
    const result: Partial<QueryResult<<%= typeName %>Query>> = {
      data: {
        __typename: 'Query',
      },
    };

    const variables: <%= typeName %>QueryVariables = {};

    const mock: MockedResponse = {
      request: {
        query: <%= upperSnakeCase %>_QUERY,
        variables,
      },
      result,
    };

    const wrapper: RenderAPI = render(
      <MockedProvider mocks={[mock]} addTypename={false}>
        <<%= typeName %> navigation={{}} />
      </MockedProvider>
    );

    // Wait for next tick for the GraphQL response.
    await timeout();

    expect(wrapper).toMatchSnapshot();
  });
});
