export { default as <%= typeName %>, <%= typeName %>Params } from './<%= fileName %>.screen';
export {
  default as <%= upperSnakeCase %>_QUERY,
  <%= className %>Operation
} from './<%= fileName %>.graphql';
