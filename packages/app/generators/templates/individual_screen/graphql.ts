import { DocumentNode } from 'graphql';
import gql from 'graphql-tag';
import { graphql, QueryOpts } from 'react-apollo';
import { NavigationInjectedProps } from 'react-navigation';
import { <%= typeName %>Query, <%= typeName %>QueryVariables } from '../../generated/graphql';
import { <%= typeName %>Params } from './<%= fileName %>.screen';

// Used to access/invalidate cache. Must be kept the same as the query name!
export const <%= className %>Operation: string = '<%= typeName %>';

// GraphQL query to fetch the data.
const <%= upperSnakeCase %>_QUERY: DocumentNode = gql`
  query <%= typeName %> {
    __typename
    # TODO Add Query.
  }
`;

// As this is a screen, the only props it can accept is the params.
type Props = <%= typeName %>Params & NavigationInjectedProps<<%= typeName %>Params>;

// tslint:disable-next-line:typedef
export const with<%= typeName %>Query = graphql<Props, <%= typeName %>Query, <%= typeName %>QueryVariables >(
  <%= upperSnakeCase %>_QUERY,
  {
    options: (props: Props): QueryOpts<<%= typeName %>QueryVariables> => ({
      variables: {},

      // Partial data allows the UI to be drawn from cached data.
      returnPartialData: true,
    }),
  }
);

export default <%= upperSnakeCase %>_QUERY;
