const Generator = require('yeoman-generator');
const path = require('path');
const fs = require('fs');
const pkgUp = require('pkg-up');
const packageJson = require('../package');

/* STRING HELPERS */
const toKebabCase = str => str.replace(/\s/g, '-').toLowerCase();
const toUpperSnakeCase = str => str.replace(/\s/g, '_').toUpperCase();
const toCamelCase = str =>
  str
    .replace(/(?:^\w|[A-Z]|\b\w)/g, (x, i) =>
      i ? x.toUpperCase() : x.toLowerCase()
    )
    .replace(/\s+/g, '');
const toCapitalisedCase = str =>
  str
    .replace(/((?:^\w|\s+\w))/g, (x, i) =>
      i ? x.toUpperCase() : x.toLowerCase()
    )
    .replace(/\s+/g, '');
const splitFromCamel = str =>
  str.replace(/([a-z]{1}[A-Z]{1})/g, x => `${x[0]} ${x[1]}`);

const projectRoot = path.dirname(pkgUp.sync());

module.exports = class extends Generator {
  async prompting() {
    this.answers = await this.prompt([
      {
        type: 'list',
        name: 'type',
        message: 'What do you want to generate?',
        choices: [
          {
            name: 'GraphQL Fragment',
            value: 'graphqlfragment',
          },
        ],
      },
      {
        type: 'string',
        name: 'name',
        transformer: str => str.toLowerCase(),
        validate: str => str.length > 0,
      },
    ]);
  }

  async writing() {
    const name = splitFromCamel(this.answers.name);
    const fileName = toKebabCase(name);
    const className = toCamelCase(name);
    const typeName = toCapitalisedCase(name);
    const upperSnakeCase = toUpperSnakeCase(name);
    const packageName = packageJson.name;

    let outputFolder;
    switch (this.answers.type) {
      case 'modal':
        outputFolder = path.join('src', 'modals');
        break;
      default:
        outputFolder = path.join('src', 'components');
        break;
    }

    // Set the output folder to the correct path
    this.destinationRoot(path.join(projectRoot, outputFolder, fileName));

    const folder = path.join(this.templatePath(), this.answers.type);

    // Loops over all files in the template folder, and tries to template it if its a file (not a folder)
    // Prefixes all file names, except index.ts
    fs.readdirSync(folder).forEach(templateFile => {
      if (!fs.lstatSync(path.join(folder, templateFile)).isDirectory()) {
        const untemplated = ['README.md', 'index.ts'];

        this.fs.copyTpl(
          path.join(folder, templateFile),
          this.destinationPath(
            untemplated.includes(templateFile)
              ? templateFile
              : `${fileName}.${templateFile}`
          ),
          { fileName, className, typeName, packageName, upperSnakeCase }
        );
      }
    });
  }
};
