import React, { Component, ReactNode } from 'react';
import { Animated, View } from 'react-native';
import {
  PanGestureHandler,
  PanGestureHandlerProperties,
  State,
} from 'react-native-gesture-handler';

const windowWidth: number = 320;

const circleRadius: number = 30;
export default class extends Component {
  private _touchX: Animated.Value = new Animated.Value(windowWidth / 2);

  private onHandlerStateChange: PanGestureHandlerProperties['onHandlerStateChange'] = ({
    nativeEvent,
  }) => {
    if (nativeEvent.state === State.END) {
      Animated.spring(this._touchX, {
        toValue: windowWidth / 2,
        bounciness: 0,
        speed: 15,
        useNativeDriver: true,
      }).start();
    }
  };

  private onGestureEvent: PanGestureHandlerProperties['onGestureEvent'] = Animated.event(
    [{ nativeEvent: { translateX: this._touchX } }],
    {
      useNativeDriver: false,
    }
  );

  public render(): ReactNode {
    return (
      <View
        style={{
          height: 150,
          justifyContent: 'center',
        }}
      >
        <PanGestureHandler
          onGestureEvent={this.onGestureEvent}
          onHandlerStateChange={this.onHandlerStateChange}
        >
          <Animated.View
            style={[
              {
                backgroundColor: '#42a5f5',
                borderRadius: circleRadius,
                height: circleRadius * 2,
                width: circleRadius * 2,
              },
              {
                transform: [
                  {
                    translateX: Animated.add(
                      this._touchX,
                      new Animated.Value(-circleRadius)
                    ),
                  },
                ],
              },
            ]}
          />
        </PanGestureHandler>
      </View>
    );
  }
}
