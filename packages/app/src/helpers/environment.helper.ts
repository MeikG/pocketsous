/* tslint:disable:no-http-string */
import Constants from 'expo-constants';
import { parse } from 'uri-js';

/**
 * Returns the API URL.
 */
export function getApiUrl(): string {
  const { releaseChannel } = Constants.manifest;
  if (releaseChannel === undefined) {
    const { host } = parse(Constants.experienceUrl);
    return `http://${host}:1337`;
  }

  // Talk to a specific version endpoint.
  if (releaseChannel.startsWith('release')) {
    const [, version] = releaseChannel.split('-');
    return `http://staging.pocketsous.mgregory.me/${version}`;
  }

  // Use the staging server.
  if (releaseChannel === 'staging') {
    return 'http://staging.pocketsous.mgregory.me';
  }

  return 'pocketsous.mgregory.me';
}
