import ApolloClient, { InMemoryCache } from 'apollo-boost';
import { getApiUrl } from './environment.helper';

const client: ApolloClient<unknown> = new ApolloClient({
  uri: `${getApiUrl()}/graphql`,
  cache: new InMemoryCache(),
});

export default client;
