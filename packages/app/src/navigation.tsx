import { NavigationContainer } from '@react-navigation/native';
import {
  createStackNavigator,
  HeaderStyleInterpolators,
  TransitionPresets,
} from '@react-navigation/stack';
import React from 'react';
import { Button, Text, View } from 'react-native';
import 'react-native-gesture-handler';

function homeScreen({ navigation }): JSX.Element {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
      <Button
        title="Go to Details"
        onPress={() => navigation.push('Details')}
      />
    </View>
  );
}

function detailsScreen({ navigation }): JSX.Element {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details </Text>
      <Button title="Go to Details" onPress={() => navigation.push('Foo')} />
    </View>
  );
}

type RootStackParamList = {
  Home: { userId: string };
  Profile: { userId: string };
  Feed: { sort: 'latest' | 'top' } | undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

export default (): JSX.Element => (
  <NavigationContainer>
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen name="Home" component={homeScreen} />
      <Stack.Screen
        name="Details"
        component={detailsScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
          headerStyleInterpolator: HeaderStyleInterpolators.forUIKit,
          gestureEnabled: true,
          cardShadowEnabled: true,
        }}
      />
      <Stack.Screen
        name="Foo"
        component={detailsScreen}
        options={{
          ...TransitionPresets.RevealFromBottomAndroid,
        }}
      />
    </Stack.Navigator>
  </NavigationContainer>
);
