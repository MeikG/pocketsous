import gql from 'graphql-tag';
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  Cursor: string,
  UUID: string,
  Datetime: string,
  Date: string,
};

export type AutocompleteIngredientsInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly text: Scalars['String'],
};

export type AutocompleteIngredientsPayload = {
  readonly __typename: 'AutocompleteIngredientsPayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly ingredients: Maybe<ReadonlyArray<Ingredient>>,
  readonly query: Maybe<Query>,
};

export type Colour = {
  readonly __typename: 'Colour',
  readonly red: Scalars['Int'],
  readonly green: Scalars['Int'],
  readonly blue: Scalars['Int'],
};

export type CreateIngredientImageInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly ingredientImage: IngredientImageInput,
};

export type CreateIngredientImagePayload = {
  readonly __typename: 'CreateIngredientImagePayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly ingredientImage: Maybe<IngredientImage>,
  readonly query: Maybe<Query>,
  readonly ingredient: Ingredient,
  readonly image: Image,
  readonly ingredientImageEdge: Maybe<IngredientImagesEdge>,
};


export type CreateIngredientImagePayloadIngredientImageEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<IngredientImagesOrderBy>>
};

export type CreateIngredientInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly ingredient: IngredientInput,
};

export type CreateIngredientPayload = {
  readonly __typename: 'CreateIngredientPayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly ingredient: Maybe<Ingredient>,
  readonly query: Maybe<Query>,
  readonly user: RegisteredUser,
  readonly ingredientEdge: Maybe<IngredientsEdge>,
};


export type CreateIngredientPayloadIngredientEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<IngredientsOrderBy>>
};

export type CreateRecipeImageInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly recipeImage: RecipeImageInput,
};

export type CreateRecipeImagePayload = {
  readonly __typename: 'CreateRecipeImagePayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly recipeImage: Maybe<RecipeImage>,
  readonly query: Maybe<Query>,
  readonly recipe: Recipe,
  readonly image: Image,
  readonly user: RegisteredUser,
  readonly recipeImageEdge: Maybe<RecipeImagesEdge>,
};


export type CreateRecipeImagePayloadRecipeImageEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<RecipeImagesOrderBy>>
};

export type CreateRecipeInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly recipe: RecipeInput,
};

export type CreateRecipeInstructionInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly recipeInstruction: RecipeInstructionInput,
};

export type CreateRecipeInstructionPayload = {
  readonly __typename: 'CreateRecipeInstructionPayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly recipeInstruction: Maybe<RecipeInstruction>,
  readonly query: Maybe<Query>,
  readonly recipe: Recipe,
  readonly recipeInstructionEdge: Maybe<RecipeInstructionsEdge>,
};


export type CreateRecipeInstructionPayloadRecipeInstructionEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<RecipeInstructionsOrderBy>>
};

export type CreateRecipePayload = {
  readonly __typename: 'CreateRecipePayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly recipe: Maybe<Recipe>,
  readonly query: Maybe<Query>,
  readonly user: RegisteredUser,
  readonly recipeEdge: Maybe<RecipesEdge>,
};


export type CreateRecipePayloadRecipeEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<RecipesOrderBy>>
};




export type DeleteIngredientByNodeIdInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly nodeId: Scalars['ID'],
};

export type DeleteIngredientImageByNodeIdInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly nodeId: Scalars['ID'],
};

export type DeleteIngredientImagePayload = {
  readonly __typename: 'DeleteIngredientImagePayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly ingredientImage: Maybe<IngredientImage>,
  readonly deletedIngredientImageNodeId: Maybe<Scalars['ID']>,
  readonly query: Maybe<Query>,
  readonly ingredient: Ingredient,
  readonly image: Image,
  readonly ingredientImageEdge: Maybe<IngredientImagesEdge>,
};


export type DeleteIngredientImagePayloadIngredientImageEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<IngredientImagesOrderBy>>
};

export type DeleteIngredientPayload = {
  readonly __typename: 'DeleteIngredientPayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly ingredient: Maybe<Ingredient>,
  readonly deletedIngredientNodeId: Maybe<Scalars['ID']>,
  readonly query: Maybe<Query>,
  readonly user: RegisteredUser,
  readonly ingredientEdge: Maybe<IngredientsEdge>,
};


export type DeleteIngredientPayloadIngredientEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<IngredientsOrderBy>>
};

export type DeleteRecipeByNodeIdInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly nodeId: Scalars['ID'],
};

export type DeleteRecipeImageByNodeIdInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly nodeId: Scalars['ID'],
};

export type DeleteRecipeImagePayload = {
  readonly __typename: 'DeleteRecipeImagePayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly recipeImage: Maybe<RecipeImage>,
  readonly deletedRecipeImageNodeId: Maybe<Scalars['ID']>,
  readonly query: Maybe<Query>,
  readonly recipe: Recipe,
  readonly image: Image,
  readonly user: RegisteredUser,
  readonly recipeImageEdge: Maybe<RecipeImagesEdge>,
};


export type DeleteRecipeImagePayloadRecipeImageEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<RecipeImagesOrderBy>>
};

export type DeleteRecipeInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly recipeId: Scalars['UUID'],
};

export type DeleteRecipeInstructionByNodeIdInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly nodeId: Scalars['ID'],
};

export type DeleteRecipeInstructionPayload = {
  readonly __typename: 'DeleteRecipeInstructionPayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly recipeInstruction: Maybe<RecipeInstruction>,
  readonly deletedRecipeInstructionNodeId: Maybe<Scalars['ID']>,
  readonly query: Maybe<Query>,
  readonly recipe: Recipe,
  readonly recipeInstructionEdge: Maybe<RecipeInstructionsEdge>,
};


export type DeleteRecipeInstructionPayloadRecipeInstructionEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<RecipeInstructionsOrderBy>>
};

export type DeleteRecipePayload = {
  readonly __typename: 'DeleteRecipePayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly recipe: Maybe<Recipe>,
  readonly deletedRecipeNodeId: Maybe<Scalars['ID']>,
  readonly query: Maybe<Query>,
  readonly user: RegisteredUser,
  readonly recipeEdge: Maybe<RecipesEdge>,
};


export type DeleteRecipePayloadRecipeEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<RecipesOrderBy>>
};

export type Image = Node & {
  readonly __typename: 'Image',
  readonly nodeId: Scalars['ID'],
  readonly imageId: Scalars['String'],
  readonly dateCreated: Scalars['Datetime'],
  readonly dominantColour: Colour,
  readonly recipeImages: RecipeImagesConnection,
  readonly ingredientImages: IngredientImagesConnection,
  readonly original: Scalars['String'],
  readonly portraitCard: Scalars['String'],
  readonly thumbnail: Scalars['String'],
  readonly titleCard: Scalars['String'],
};


export type ImageRecipeImagesArgs = {
  first: Maybe<Scalars['Int']>,
  last: Maybe<Scalars['Int']>,
  offset: Maybe<Scalars['Int']>,
  before: Maybe<Scalars['Cursor']>,
  after: Maybe<Scalars['Cursor']>,
  orderBy?: Maybe<ReadonlyArray<RecipeImagesOrderBy>>,
  condition: Maybe<RecipeImageCondition>
};


export type ImageIngredientImagesArgs = {
  first: Maybe<Scalars['Int']>,
  last: Maybe<Scalars['Int']>,
  offset: Maybe<Scalars['Int']>,
  before: Maybe<Scalars['Cursor']>,
  after: Maybe<Scalars['Cursor']>,
  orderBy?: Maybe<ReadonlyArray<IngredientImagesOrderBy>>,
  condition: Maybe<IngredientImageCondition>
};

export type Ingredient = Node & {
  readonly __typename: 'Ingredient',
  readonly nodeId: Scalars['ID'],
  readonly ingredientId: Scalars['UUID'],
  readonly dateCreated: Scalars['Datetime'],
  readonly userId: Maybe<Scalars['UUID']>,
  readonly ingredientName: Scalars['String'],
  readonly user: Maybe<RegisteredUser>,
  readonly recipeIngredients: RecipeIngredientsConnection,
  readonly image: Maybe<IngredientImage>,
};


export type IngredientRecipeIngredientsArgs = {
  first: Maybe<Scalars['Int']>,
  last: Maybe<Scalars['Int']>,
  offset: Maybe<Scalars['Int']>,
  before: Maybe<Scalars['Cursor']>,
  after: Maybe<Scalars['Cursor']>,
  orderBy?: Maybe<ReadonlyArray<RecipeIngredientsOrderBy>>,
  condition: Maybe<RecipeIngredientCondition>
};

export type IngredientCondition = {
  readonly ingredientId: Maybe<Scalars['UUID']>,
  readonly userId: Maybe<Scalars['UUID']>,
};

export type IngredientImage = Node & {
  readonly __typename: 'IngredientImage',
  readonly nodeId: Scalars['ID'],
  readonly ingredientId: Scalars['UUID'],
  readonly imageId: Scalars['String'],
  readonly dateCreated: Scalars['Datetime'],
  readonly userId: Maybe<Scalars['UUID']>,
  readonly ingredient: Ingredient,
  readonly image: Image,
};

export type IngredientImageCondition = {
  readonly ingredientId: Maybe<Scalars['UUID']>,
  readonly imageId: Maybe<Scalars['String']>,
  readonly userId: Maybe<Scalars['UUID']>,
};

export type IngredientImageInput = {
  readonly ingredientId: Scalars['UUID'],
  readonly imageId: Scalars['String'],
};

export type IngredientImagesConnection = {
  readonly __typename: 'IngredientImagesConnection',
  readonly nodes: ReadonlyArray<IngredientImage>,
  readonly edges: ReadonlyArray<IngredientImagesEdge>,
  readonly pageInfo: PageInfo,
  readonly totalCount: Scalars['Int'],
};

export type IngredientImagesEdge = {
  readonly __typename: 'IngredientImagesEdge',
  readonly cursor: Maybe<Scalars['Cursor']>,
  readonly node: IngredientImage,
};

export enum IngredientImagesOrderBy {
  Natural = 'NATURAL',
  IngredientIdAsc = 'INGREDIENT_ID_ASC',
  IngredientIdDesc = 'INGREDIENT_ID_DESC',
  ImageIdAsc = 'IMAGE_ID_ASC',
  ImageIdDesc = 'IMAGE_ID_DESC',
  UserIdAsc = 'USER_ID_ASC',
  UserIdDesc = 'USER_ID_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

export type IngredientInput = {
  readonly ingredientId: Maybe<Scalars['UUID']>,
  readonly ingredientName: Scalars['String'],
};

export type IngredientPatch = {
  readonly ingredientId: Maybe<Scalars['UUID']>,
  readonly ingredientName: Maybe<Scalars['String']>,
};

export type IngredientsConnection = {
  readonly __typename: 'IngredientsConnection',
  readonly nodes: ReadonlyArray<Ingredient>,
  readonly edges: ReadonlyArray<IngredientsEdge>,
  readonly pageInfo: PageInfo,
  readonly totalCount: Scalars['Int'],
};

export type IngredientsEdge = {
  readonly __typename: 'IngredientsEdge',
  readonly cursor: Maybe<Scalars['Cursor']>,
  readonly node: Ingredient,
};

export enum IngredientsOrderBy {
  Natural = 'NATURAL',
  IngredientIdAsc = 'INGREDIENT_ID_ASC',
  IngredientIdDesc = 'INGREDIENT_ID_DESC',
  UserIdAsc = 'USER_ID_ASC',
  UserIdDesc = 'USER_ID_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

export enum InstructionType {
  MealPrep = 'MEAL_PREP',
  Instruction = 'INSTRUCTION'
}

export type Mutation = {
  readonly __typename: 'Mutation',
  readonly createIngredient: Maybe<CreateIngredientPayload>,
  readonly createIngredientImage: Maybe<CreateIngredientImagePayload>,
  readonly createRecipe: Maybe<CreateRecipePayload>,
  readonly createRecipeImage: Maybe<CreateRecipeImagePayload>,
  readonly createRecipeInstruction: Maybe<CreateRecipeInstructionPayload>,
  readonly updateIngredientByNodeId: Maybe<UpdateIngredientPayload>,
  readonly updateRecipeByNodeId: Maybe<UpdateRecipePayload>,
  readonly updateRecipe: Maybe<UpdateRecipePayload>,
  readonly updateRecipeInstructionByNodeId: Maybe<UpdateRecipeInstructionPayload>,
  readonly updateUserPreferenceByNodeId: Maybe<UpdateUserPreferencePayload>,
  readonly deleteIngredientByNodeId: Maybe<DeleteIngredientPayload>,
  readonly deleteIngredientImageByNodeId: Maybe<DeleteIngredientImagePayload>,
  readonly deleteRecipeByNodeId: Maybe<DeleteRecipePayload>,
  readonly deleteRecipe: Maybe<DeleteRecipePayload>,
  readonly deleteRecipeImageByNodeId: Maybe<DeleteRecipeImagePayload>,
  readonly deleteRecipeInstructionByNodeId: Maybe<DeleteRecipeInstructionPayload>,
  readonly autocompleteIngredients: Maybe<AutocompleteIngredientsPayload>,
};


export type MutationCreateIngredientArgs = {
  input: CreateIngredientInput
};


export type MutationCreateIngredientImageArgs = {
  input: CreateIngredientImageInput
};


export type MutationCreateRecipeArgs = {
  input: CreateRecipeInput
};


export type MutationCreateRecipeImageArgs = {
  input: CreateRecipeImageInput
};


export type MutationCreateRecipeInstructionArgs = {
  input: CreateRecipeInstructionInput
};


export type MutationUpdateIngredientByNodeIdArgs = {
  input: UpdateIngredientByNodeIdInput
};


export type MutationUpdateRecipeByNodeIdArgs = {
  input: UpdateRecipeByNodeIdInput
};


export type MutationUpdateRecipeArgs = {
  input: UpdateRecipeInput
};


export type MutationUpdateRecipeInstructionByNodeIdArgs = {
  input: UpdateRecipeInstructionByNodeIdInput
};


export type MutationUpdateUserPreferenceByNodeIdArgs = {
  input: UpdateUserPreferenceByNodeIdInput
};


export type MutationDeleteIngredientByNodeIdArgs = {
  input: DeleteIngredientByNodeIdInput
};


export type MutationDeleteIngredientImageByNodeIdArgs = {
  input: DeleteIngredientImageByNodeIdInput
};


export type MutationDeleteRecipeByNodeIdArgs = {
  input: DeleteRecipeByNodeIdInput
};


export type MutationDeleteRecipeArgs = {
  input: DeleteRecipeInput
};


export type MutationDeleteRecipeImageByNodeIdArgs = {
  input: DeleteRecipeImageByNodeIdInput
};


export type MutationDeleteRecipeInstructionByNodeIdArgs = {
  input: DeleteRecipeInstructionByNodeIdInput
};


export type MutationAutocompleteIngredientsArgs = {
  input: AutocompleteIngredientsInput
};

export type Node = {
  readonly nodeId: Scalars['ID'],
};

export type PageInfo = {
  readonly __typename: 'PageInfo',
  readonly hasNextPage: Scalars['Boolean'],
  readonly hasPreviousPage: Scalars['Boolean'],
  readonly startCursor: Maybe<Scalars['Cursor']>,
  readonly endCursor: Maybe<Scalars['Cursor']>,
};

export type Query = Node & {
  readonly __typename: 'Query',
  readonly query: Query,
  readonly nodeId: Scalars['ID'],
  readonly node: Maybe<Node>,
  readonly ingredients: Maybe<IngredientsConnection>,
  readonly recipes: Maybe<RecipesConnection>,
  readonly recipeInstructions: Maybe<RecipeInstructionsConnection>,
  readonly image: Maybe<Image>,
  readonly recipe: Maybe<Recipe>,
  readonly getCurrentUser: Maybe<RegisteredUser>,
  readonly textToQuery: Maybe<Scalars['String']>,
  readonly imageByNodeId: Maybe<Image>,
  readonly ingredientByNodeId: Maybe<Ingredient>,
  readonly ingredientImageByNodeId: Maybe<IngredientImage>,
  readonly recipeByNodeId: Maybe<Recipe>,
  readonly recipeImageByNodeId: Maybe<RecipeImage>,
  readonly recipeIngredientByNodeId: Maybe<RecipeIngredient>,
  readonly recipeInstructionByNodeId: Maybe<RecipeInstruction>,
  readonly registeredUserByNodeId: Maybe<RegisteredUser>,
  readonly userPreferenceByNodeId: Maybe<UserPreference>,
};


export type QueryNodeArgs = {
  nodeId: Scalars['ID']
};


export type QueryIngredientsArgs = {
  first: Maybe<Scalars['Int']>,
  last: Maybe<Scalars['Int']>,
  offset: Maybe<Scalars['Int']>,
  before: Maybe<Scalars['Cursor']>,
  after: Maybe<Scalars['Cursor']>,
  orderBy?: Maybe<ReadonlyArray<IngredientsOrderBy>>,
  condition: Maybe<IngredientCondition>
};


export type QueryRecipesArgs = {
  first: Maybe<Scalars['Int']>,
  last: Maybe<Scalars['Int']>,
  offset: Maybe<Scalars['Int']>,
  before: Maybe<Scalars['Cursor']>,
  after: Maybe<Scalars['Cursor']>,
  orderBy?: Maybe<ReadonlyArray<RecipesOrderBy>>,
  condition: Maybe<RecipeCondition>
};


export type QueryRecipeInstructionsArgs = {
  first: Maybe<Scalars['Int']>,
  last: Maybe<Scalars['Int']>,
  offset: Maybe<Scalars['Int']>,
  before: Maybe<Scalars['Cursor']>,
  after: Maybe<Scalars['Cursor']>,
  orderBy?: Maybe<ReadonlyArray<RecipeInstructionsOrderBy>>,
  condition: Maybe<RecipeInstructionCondition>
};


export type QueryImageArgs = {
  imageId: Scalars['String']
};


export type QueryRecipeArgs = {
  recipeId: Scalars['UUID']
};


export type QueryTextToQueryArgs = {
  searchText: Maybe<Scalars['String']>
};


export type QueryImageByNodeIdArgs = {
  nodeId: Scalars['ID']
};


export type QueryIngredientByNodeIdArgs = {
  nodeId: Scalars['ID']
};


export type QueryIngredientImageByNodeIdArgs = {
  nodeId: Scalars['ID']
};


export type QueryRecipeByNodeIdArgs = {
  nodeId: Scalars['ID']
};


export type QueryRecipeImageByNodeIdArgs = {
  nodeId: Scalars['ID']
};


export type QueryRecipeIngredientByNodeIdArgs = {
  nodeId: Scalars['ID']
};


export type QueryRecipeInstructionByNodeIdArgs = {
  nodeId: Scalars['ID']
};


export type QueryRegisteredUserByNodeIdArgs = {
  nodeId: Scalars['ID']
};


export type QueryUserPreferenceByNodeIdArgs = {
  nodeId: Scalars['ID']
};

export type Recipe = Node & {
  readonly __typename: 'Recipe',
  readonly nodeId: Scalars['ID'],
  readonly recipeId: Scalars['UUID'],
  readonly dateCreated: Scalars['Datetime'],
  readonly userId: Scalars['UUID'],
  readonly recipeName: Scalars['String'],
  readonly recipeType: RecipeType,
  readonly description: Scalars['String'],
  readonly user: RegisteredUser,
  readonly ingredients: RecipeIngredientsConnection,
  readonly instructions: RecipeInstructionsConnection,
  readonly images: RecipeImagesConnection,
};


export type RecipeIngredientsArgs = {
  first: Maybe<Scalars['Int']>,
  last: Maybe<Scalars['Int']>,
  offset: Maybe<Scalars['Int']>,
  before: Maybe<Scalars['Cursor']>,
  after: Maybe<Scalars['Cursor']>,
  orderBy?: Maybe<ReadonlyArray<RecipeIngredientsOrderBy>>,
  condition: Maybe<RecipeIngredientCondition>
};


export type RecipeInstructionsArgs = {
  first: Maybe<Scalars['Int']>,
  last: Maybe<Scalars['Int']>,
  offset: Maybe<Scalars['Int']>,
  before: Maybe<Scalars['Cursor']>,
  after: Maybe<Scalars['Cursor']>,
  orderBy?: Maybe<ReadonlyArray<RecipeInstructionsOrderBy>>,
  condition: Maybe<RecipeInstructionCondition>
};


export type RecipeImagesArgs = {
  first: Maybe<Scalars['Int']>,
  last: Maybe<Scalars['Int']>,
  offset: Maybe<Scalars['Int']>,
  before: Maybe<Scalars['Cursor']>,
  after: Maybe<Scalars['Cursor']>,
  orderBy?: Maybe<ReadonlyArray<RecipeImagesOrderBy>>,
  condition: Maybe<RecipeImageCondition>
};

export type RecipeCondition = {
  readonly recipeId: Maybe<Scalars['UUID']>,
  readonly userId: Maybe<Scalars['UUID']>,
};

export type RecipeImage = Node & {
  readonly __typename: 'RecipeImage',
  readonly nodeId: Scalars['ID'],
  readonly recipeId: Scalars['UUID'],
  readonly imageId: Scalars['String'],
  readonly dateCreated: Scalars['Datetime'],
  readonly ordering: Scalars['Int'],
  readonly userId: Scalars['UUID'],
  readonly recipe: Recipe,
  readonly image: Image,
  readonly user: RegisteredUser,
};

export type RecipeImageCondition = {
  readonly recipeId: Maybe<Scalars['UUID']>,
  readonly imageId: Maybe<Scalars['String']>,
  readonly userId: Maybe<Scalars['UUID']>,
};

export type RecipeImageInput = {
  readonly recipeId: Scalars['UUID'],
  readonly imageId: Scalars['String'],
  readonly ordering: Scalars['Int'],
};

export type RecipeImagesConnection = {
  readonly __typename: 'RecipeImagesConnection',
  readonly nodes: ReadonlyArray<RecipeImage>,
  readonly edges: ReadonlyArray<RecipeImagesEdge>,
  readonly pageInfo: PageInfo,
  readonly totalCount: Scalars['Int'],
};

export type RecipeImagesEdge = {
  readonly __typename: 'RecipeImagesEdge',
  readonly cursor: Maybe<Scalars['Cursor']>,
  readonly node: RecipeImage,
};

export enum RecipeImagesOrderBy {
  Natural = 'NATURAL',
  RecipeIdAsc = 'RECIPE_ID_ASC',
  RecipeIdDesc = 'RECIPE_ID_DESC',
  ImageIdAsc = 'IMAGE_ID_ASC',
  ImageIdDesc = 'IMAGE_ID_DESC',
  UserIdAsc = 'USER_ID_ASC',
  UserIdDesc = 'USER_ID_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

export type RecipeIngredient = Node & {
  readonly __typename: 'RecipeIngredient',
  readonly nodeId: Scalars['ID'],
  readonly recipeId: Scalars['UUID'],
  readonly ingredientId: Scalars['UUID'],
  readonly dateCreated: Scalars['Datetime'],
  readonly quantity: Scalars['String'],
  readonly ordering: Scalars['Int'],
  readonly recipe: Recipe,
  readonly ingredient: Ingredient,
};

export type RecipeIngredientCondition = {
  readonly recipeId: Maybe<Scalars['UUID']>,
  readonly ingredientId: Maybe<Scalars['UUID']>,
};

export type RecipeIngredientsConnection = {
  readonly __typename: 'RecipeIngredientsConnection',
  readonly nodes: ReadonlyArray<RecipeIngredient>,
  readonly edges: ReadonlyArray<RecipeIngredientsEdge>,
  readonly pageInfo: PageInfo,
  readonly totalCount: Scalars['Int'],
};

export type RecipeIngredientsEdge = {
  readonly __typename: 'RecipeIngredientsEdge',
  readonly cursor: Maybe<Scalars['Cursor']>,
  readonly node: RecipeIngredient,
};

export enum RecipeIngredientsOrderBy {
  Natural = 'NATURAL',
  RecipeIdAsc = 'RECIPE_ID_ASC',
  RecipeIdDesc = 'RECIPE_ID_DESC',
  IngredientIdAsc = 'INGREDIENT_ID_ASC',
  IngredientIdDesc = 'INGREDIENT_ID_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

export type RecipeInput = {
  readonly recipeName: Scalars['String'],
  readonly recipeType: RecipeType,
  readonly description: Maybe<Scalars['String']>,
};

export type RecipeInstruction = Node & {
  readonly __typename: 'RecipeInstruction',
  readonly nodeId: Scalars['ID'],
  readonly recipeInstructionId: Scalars['UUID'],
  readonly recipeId: Scalars['UUID'],
  readonly dateCreated: Scalars['Datetime'],
  readonly userId: Scalars['UUID'],
  readonly description: Scalars['String'],
  readonly instructionType: InstructionType,
  readonly ordering: Scalars['Int'],
  readonly recipe: Recipe,
};

export type RecipeInstructionCondition = {
  readonly recipeInstructionId: Maybe<Scalars['UUID']>,
  readonly recipeId: Maybe<Scalars['UUID']>,
  readonly userId: Maybe<Scalars['UUID']>,
};

export type RecipeInstructionInput = {
  readonly recipeId: Scalars['UUID'],
  readonly description: Scalars['String'],
  readonly instructionType: InstructionType,
  readonly ordering: Scalars['Int'],
};

export type RecipeInstructionPatch = {
  readonly description: Maybe<Scalars['String']>,
  readonly instructionType: Maybe<InstructionType>,
  readonly ordering: Maybe<Scalars['Int']>,
};

export type RecipeInstructionsConnection = {
  readonly __typename: 'RecipeInstructionsConnection',
  readonly nodes: ReadonlyArray<RecipeInstruction>,
  readonly edges: ReadonlyArray<RecipeInstructionsEdge>,
  readonly pageInfo: PageInfo,
  readonly totalCount: Scalars['Int'],
};

export type RecipeInstructionsEdge = {
  readonly __typename: 'RecipeInstructionsEdge',
  readonly cursor: Maybe<Scalars['Cursor']>,
  readonly node: RecipeInstruction,
};

export enum RecipeInstructionsOrderBy {
  Natural = 'NATURAL',
  RecipeInstructionIdAsc = 'RECIPE_INSTRUCTION_ID_ASC',
  RecipeInstructionIdDesc = 'RECIPE_INSTRUCTION_ID_DESC',
  RecipeIdAsc = 'RECIPE_ID_ASC',
  RecipeIdDesc = 'RECIPE_ID_DESC',
  UserIdAsc = 'USER_ID_ASC',
  UserIdDesc = 'USER_ID_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

export type RecipePatch = {
  readonly recipeName: Maybe<Scalars['String']>,
  readonly recipeType: Maybe<RecipeType>,
  readonly description: Maybe<Scalars['String']>,
};

export type RecipesConnection = {
  readonly __typename: 'RecipesConnection',
  readonly nodes: ReadonlyArray<Recipe>,
  readonly edges: ReadonlyArray<RecipesEdge>,
  readonly pageInfo: PageInfo,
  readonly totalCount: Scalars['Int'],
};

export type RecipesEdge = {
  readonly __typename: 'RecipesEdge',
  readonly cursor: Maybe<Scalars['Cursor']>,
  readonly node: Recipe,
};

export enum RecipesOrderBy {
  Natural = 'NATURAL',
  RecipeIdAsc = 'RECIPE_ID_ASC',
  RecipeIdDesc = 'RECIPE_ID_DESC',
  UserIdAsc = 'USER_ID_ASC',
  UserIdDesc = 'USER_ID_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

export enum RecipeType {
  Meal = 'MEAL',
  Side = 'SIDE'
}

export type RegisteredUser = Node & {
  readonly __typename: 'RegisteredUser',
  readonly nodeId: Scalars['ID'],
  readonly userId: Scalars['UUID'],
  readonly dateCreated: Scalars['Datetime'],
  readonly photo: Maybe<Scalars['String']>,
  readonly displayName: Scalars['String'],
  readonly role: Role,
  readonly userPreference: Maybe<UserPreference>,
  readonly recipes: RecipesConnection,
  readonly ingredients: IngredientsConnection,
  readonly recipeImages: RecipeImagesConnection,
};


export type RegisteredUserRecipesArgs = {
  first: Maybe<Scalars['Int']>,
  last: Maybe<Scalars['Int']>,
  offset: Maybe<Scalars['Int']>,
  before: Maybe<Scalars['Cursor']>,
  after: Maybe<Scalars['Cursor']>,
  orderBy?: Maybe<ReadonlyArray<RecipesOrderBy>>,
  condition: Maybe<RecipeCondition>
};


export type RegisteredUserIngredientsArgs = {
  first: Maybe<Scalars['Int']>,
  last: Maybe<Scalars['Int']>,
  offset: Maybe<Scalars['Int']>,
  before: Maybe<Scalars['Cursor']>,
  after: Maybe<Scalars['Cursor']>,
  orderBy?: Maybe<ReadonlyArray<IngredientsOrderBy>>,
  condition: Maybe<IngredientCondition>
};


export type RegisteredUserRecipeImagesArgs = {
  first: Maybe<Scalars['Int']>,
  last: Maybe<Scalars['Int']>,
  offset: Maybe<Scalars['Int']>,
  before: Maybe<Scalars['Cursor']>,
  after: Maybe<Scalars['Cursor']>,
  orderBy?: Maybe<ReadonlyArray<RecipeImagesOrderBy>>,
  condition: Maybe<RecipeImageCondition>
};

export enum Role {
  Anonymous = 'ANONYMOUS',
  Authenticated = 'AUTHENTICATED',
  Admin = 'ADMIN'
}

export type UpdateIngredientByNodeIdInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly nodeId: Scalars['ID'],
  readonly patch: IngredientPatch,
};

export type UpdateIngredientPayload = {
  readonly __typename: 'UpdateIngredientPayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly ingredient: Maybe<Ingredient>,
  readonly query: Maybe<Query>,
  readonly user: RegisteredUser,
  readonly ingredientEdge: Maybe<IngredientsEdge>,
};


export type UpdateIngredientPayloadIngredientEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<IngredientsOrderBy>>
};

export type UpdateRecipeByNodeIdInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly nodeId: Scalars['ID'],
  readonly patch: RecipePatch,
};

export type UpdateRecipeInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly patch: RecipePatch,
  readonly recipeId: Scalars['UUID'],
};

export type UpdateRecipeInstructionByNodeIdInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly nodeId: Scalars['ID'],
  readonly patch: RecipeInstructionPatch,
};

export type UpdateRecipeInstructionPayload = {
  readonly __typename: 'UpdateRecipeInstructionPayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly recipeInstruction: Maybe<RecipeInstruction>,
  readonly query: Maybe<Query>,
  readonly recipe: Recipe,
  readonly recipeInstructionEdge: Maybe<RecipeInstructionsEdge>,
};


export type UpdateRecipeInstructionPayloadRecipeInstructionEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<RecipeInstructionsOrderBy>>
};

export type UpdateRecipePayload = {
  readonly __typename: 'UpdateRecipePayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly recipe: Maybe<Recipe>,
  readonly query: Maybe<Query>,
  readonly user: RegisteredUser,
  readonly recipeEdge: Maybe<RecipesEdge>,
};


export type UpdateRecipePayloadRecipeEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<RecipesOrderBy>>
};

export type UpdateUserPreferenceByNodeIdInput = {
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly nodeId: Scalars['ID'],
  readonly patch: UserPreferencePatch,
};

export type UpdateUserPreferencePayload = {
  readonly __typename: 'UpdateUserPreferencePayload',
  readonly clientMutationId: Maybe<Scalars['String']>,
  readonly userPreference: Maybe<UserPreference>,
  readonly query: Maybe<Query>,
  readonly user: RegisteredUser,
  readonly userPreferenceEdge: Maybe<UserPreferencesEdge>,
};


export type UpdateUserPreferencePayloadUserPreferenceEdgeArgs = {
  orderBy?: Maybe<ReadonlyArray<UserPreferencesOrderBy>>
};

export type UserPreference = Node & {
  readonly __typename: 'UserPreference',
  readonly nodeId: Scalars['ID'],
  readonly userId: Scalars['UUID'],
  readonly dateCreated: Scalars['Datetime'],
  readonly firstTime: Scalars['Boolean'],
  readonly marketing: Scalars['Boolean'],
  readonly marketingAccepted: Maybe<Scalars['Date']>,
  readonly user: RegisteredUser,
};

export type UserPreferencePatch = {
  readonly userId: Maybe<Scalars['UUID']>,
  readonly dateCreated: Maybe<Scalars['Datetime']>,
  readonly firstTime: Maybe<Scalars['Boolean']>,
  readonly marketing: Maybe<Scalars['Boolean']>,
  readonly marketingAccepted: Maybe<Scalars['Date']>,
};

export type UserPreferencesEdge = {
  readonly __typename: 'UserPreferencesEdge',
  readonly cursor: Maybe<Scalars['Cursor']>,
  readonly node: UserPreference,
};

export enum UserPreferencesOrderBy {
  Natural = 'NATURAL',
  UserIdAsc = 'USER_ID_ASC',
  UserIdDesc = 'USER_ID_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}


export type TestingFragment = { readonly __typename: 'Query' };


      export interface IntrospectionResultData {
        __schema: {
          types: {
            kind: string;
            name: string;
            possibleTypes: {
              name: string;
            }[];
          }[];
        };
      }
      const result: IntrospectionResultData = {
  "__schema": {
    "types": [
      {
        "kind": "INTERFACE",
        "name": "Node",
        "possibleTypes": [
          {
            "name": "Query"
          },
          {
            "name": "Ingredient"
          },
          {
            "name": "RegisteredUser"
          },
          {
            "name": "UserPreference"
          },
          {
            "name": "Recipe"
          },
          {
            "name": "RecipeIngredient"
          },
          {
            "name": "RecipeInstruction"
          },
          {
            "name": "RecipeImage"
          },
          {
            "name": "Image"
          },
          {
            "name": "IngredientImage"
          }
        ]
      }
    ]
  }
};
      export default result;
    
export const TestingFragmentDoc = gql`
    fragment Testing on Query {
  __typename
}
    `;