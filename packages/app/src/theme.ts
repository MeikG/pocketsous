import { DefaultTheme, Theme } from 'react-native-paper';

const theme: Theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#24828F',
    accent: '#9C4270',
    background: '#FAFAFA',
    text: '#111111',
  },
};

export default theme;
