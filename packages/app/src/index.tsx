import { ApolloProvider } from '@apollo/react-hoc';
import React, { Component, ReactNode } from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import Navigation from './navigation';

import client from './helpers/apollo.helper';
import theme from './theme';

export default class extends Component {
  public render(): ReactNode {
    return (
      <ApolloProvider client={client}>
        <PaperProvider theme={theme}>
          <Navigation />
        </PaperProvider>
      </ApolloProvider>
    );
  }
}
