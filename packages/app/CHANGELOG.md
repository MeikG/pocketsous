# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.3.1](https://gitlab.com/MeikG/pocketsous/compare/v1.3.0...v1.3.1) (2020-05-14)

**Note:** Version bump only for package @pocketsous/app





# [1.3.0](https://gitlab.com/MeikG/pocketsous/compare/v1.1.0...v1.3.0) (2020-05-13)

**Note:** Version bump only for package @pocketsous/app





# [1.2.0](https://gitlab.com/MeikG/pocketsous/compare/v1.1.0...v1.2.0) (2020-05-13)

**Note:** Version bump only for package @pocketsous/app





# 1.1.0 (2020-04-14)


### Features

* add Dockerfile to app and build as a static website inside an nginx container ([35999bd](https://gitlab.com/MeikG/pocketsous/commit/35999bd1419aee88e1d3eb27b2843ee3155f1a9e))
