const request = require('supertest');
const app = require('./app');
const { mockPgQuery } = require('pg');
const { mockPutObject } = require('minio');
const { mockJwtVerify } = require('jsonwebtoken');
const path = require('path');

jest.mock('./manipulations.js', () => ({
  getHash: jest.fn(input => input),
  getImageVariants: jest.fn(input => ({
    originalImage: `original_${input}`,
    titleCard: `title_${input}`,
    portraitCard: `portrait_${input}`,
    thumbnail: `thumbnail_${input}`,
    dominantColor: `dominant_${input}`,
  })),
}));
jest.mock('./package.json', () => ({
  version: 'foo',
}));

const { IMAGE_UPLOAD_ENDPOINT, VERSION_ENDPOINT } = process.env;

beforeEach(() => {
  jest.clearAllMocks();
  mockPgQuery.mockReset();
  mockPutObject.mockReset();
});

describe('Authentication', () => {
  it('should fail if the Authorization header is missing', async () => {
    const response = await request(app).post(IMAGE_UPLOAD_ENDPOINT);

    expect(response.statusCode).toBe(401);
    expect(mockJwtVerify).not.toBeCalled();
  });

  it('should fail if the authorization header doesn\'t start with "Bearer"', async () => {
    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'foo');

    expect(response.statusCode).toBe(401);
    expect(mockJwtVerify).not.toBeCalled();
  });

  it('should return a 500 if the first database call fails', async () => {
    mockPgQuery.mockImplementationOnce(() => {
      throw new Error('database error');
    });

    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer exists')
      .attach('image', path.join(__dirname, '__fixtures__', 'micro.jpg'));

    expect(response.statusCode).toBe(500);
  });

  it('return a 500 if minio fails to put an object', async () => {
    mockPutObject.mockImplementationOnce(async () => {});
    mockPutObject.mockImplementationOnce(async () => {});
    mockPutObject.mockImplementationOnce(() => {
      throw new Error('failure');
    });
    mockPutObject.mockImplementationOnce(async () => {});

    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer with_500')
      .attach('image', path.join(__dirname, '__fixtures__', 'micro.jpg'));

    expect(response.statusCode).toBe(500);

    // Expect that no data has been entered into the database.
    expect(mockPgQuery).toBeCalledTimes(1);
  });

  it('should fail if the JWT is invalid', async () => {
    mockJwtVerify.mockImplementationOnce(() => {
      throw new Error('error');
    });

    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer invalid_jwt')
      .attach('image', path.join(__dirname, '__fixtures__', 'micro.jpg'));

    expect(response.statusCode).toBe(403);
    expect(mockJwtVerify).toBeCalled();
  });

  it('should return an image ID if the image hash has already been uploaded', async () => {
    mockPgQuery.mockImplementationOnce(() => ({
      rows: [{ image_id: 'exists' }],
    }));

    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer exists')
      .attach('image', path.join(__dirname, '__fixtures__', 'micro.jpg'));

    expect(mockJwtVerify).toBeCalled();
    expect();
    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual({ id: 'exists' });
  });

  it('should add a new entry into the database, process the images, and return the image ID', async () => {
    // Initial query which returns zero rows.
    mockPgQuery.mockImplementationOnce(() => ({
      rows: [],
    }));

    // Second query where the row is inserted into the database.
    mockPgQuery.mockImplementationOnce(() => ({
      rows: [{ image_id: 'newly_created' }],
    }));

    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer no_exists')
      .attach('image', path.join(__dirname, '__fixtures__', 'micro.jpg'));

    expect(response.statusCode).toBe(200);

    // Expect minio's putObject method to be called 4 times.
    expect(mockPutObject).toBeCalledTimes(4);

    // Expect two database calls to have been made, one to query and another to insert.
    expect(mockPgQuery).toBeCalledTimes(2);

    expect(response.body).toEqual({ id: 'newly_created' });
  });
});

describe('Version Endpoint', () => {
  it('should return the current version', async () => {
    const response = await request(app).get(VERSION_ENDPOINT);

    expect(response.statusCode).toBe(200);

    expect(response.body).toEqual({
      version: 'foo',
    });
  });
});
