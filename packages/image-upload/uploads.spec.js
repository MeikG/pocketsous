const request = require('supertest');
const app = require('./app');
const { mockPgQuery } = require('pg');
const { mockPutObject } = require('minio');
const jwt = require('jsonwebtoken');
const path = require('path');

const { IMAGE_UPLOAD_ENDPOINT } = process.env;

beforeAll(function(done) {
  app.on('serverReady', function() {
    done();
  });
});

beforeEach(() => {
  jest.clearAllMocks();
  mockPgQuery.mockReset();
  mockPutObject.mockReset();
});

describe('Image Uploads', () => {
  it('should process uploaded horizontal JPEG files', async () => {
    const imageHash = 'cyg00000000';

    mockPgQuery.mockReturnValueOnce({
      rows: [],
    });

    mockPgQuery.mockReturnValueOnce({
      rows: [{ image_id: 'exists' }],
    });
    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer no_exists')
      .attach(
        'image',
        path.join(__dirname, '__fixtures__', 'test_horizontal.jpg')
      );

    expect(mockPgQuery).toHaveBeenCalledTimes(2);
    expect(mockPgQuery.mock.calls[0][1]).toEqual([imageHash]);
    expect(mockPgQuery.mock.calls[1][1]).toEqual([
      imageHash,
      undefined,
      '(235, 235, 235)',
    ]);

    // Expect that Minio's putObject method has been called 4 times to store the image.
    expect(mockPutObject).toHaveBeenCalledTimes(4);

    expect(response.statusCode).toBe(200);
  });

  it('should process uploaded vertical JPEG files', async () => {
    const imageHash = '93001300000';

    mockPgQuery.mockReturnValueOnce({
      rows: [],
    });

    mockPgQuery.mockReturnValueOnce({
      rows: [{ image_id: 'exists' }],
    });
    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer no_exists')
      .attach(
        'image',
        path.join(__dirname, '__fixtures__', 'test_vertical.jpg')
      );

    expect(mockPgQuery).toHaveBeenCalledTimes(2);
    expect(mockPgQuery.mock.calls[0][1]).toEqual([imageHash]);
    expect(mockPgQuery.mock.calls[1][1]).toEqual([
      imageHash,
      undefined,
      '(235, 235, 235)',
    ]);

    // Expect that Minio's putObject method has been called 4 times to store the image.
    expect(mockPutObject).toHaveBeenCalledTimes(4);

    expect(response.statusCode).toBe(200);
  });

  it('should process uploaded horizontal PNG files', async () => {
    const imageHash = 'cyg00000000';

    mockPgQuery.mockReturnValueOnce({
      rows: [],
    });

    mockPgQuery.mockReturnValueOnce({
      rows: [{ image_id: 'exists' }],
    });
    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer no_exists')
      .attach(
        'image',
        path.join(__dirname, '__fixtures__', 'test_horizontal.png')
      );

    expect(mockPgQuery).toHaveBeenCalledTimes(2);
    expect(mockPgQuery.mock.calls[0][1]).toEqual([imageHash]);
    expect(mockPgQuery.mock.calls[1][1]).toEqual([
      imageHash,
      undefined,
      '(235, 235, 235)',
    ]);

    // Expect that Minio's putObject method has been called 4 times to store the image.
    expect(mockPutObject).toHaveBeenCalledTimes(4);

    expect(response.statusCode).toBe(200);
  });

  it('should process uploaded vertical PNG files', async () => {
    const imageHash = '93001300000';

    mockPgQuery.mockReturnValueOnce({
      rows: [],
    });

    mockPgQuery.mockReturnValueOnce({
      rows: [{ image_id: 'exists' }],
    });
    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer no_exists')
      .attach(
        'image',
        path.join(__dirname, '__fixtures__', 'test_vertical.png')
      );

    expect(mockPgQuery).toHaveBeenCalledTimes(2);
    expect(mockPgQuery.mock.calls[0][1]).toEqual([imageHash]);
    expect(mockPgQuery.mock.calls[1][1]).toEqual([
      imageHash,
      undefined,
      '(235, 235, 235)',
    ]);

    // Expect that Minio's putObject method has been called 4 times to store the image.
    expect(mockPutObject).toHaveBeenCalledTimes(4);

    expect(response.statusCode).toBe(200);
  });

  it('should process uploaded horizontal GIF files', async () => {
    const imageHash = 'cyg00000000';

    mockPgQuery.mockReturnValueOnce({
      rows: [],
    });

    mockPgQuery.mockReturnValueOnce({
      rows: [{ image_id: 'exists' }],
    });
    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer no_exists')
      .attach(
        'image',
        path.join(__dirname, '__fixtures__', 'test_horizontal.gif')
      );

    expect(mockPgQuery).toHaveBeenCalledTimes(2);
    expect(mockPgQuery.mock.calls[0][1]).toEqual([imageHash]);
    expect(mockPgQuery.mock.calls[1][1]).toEqual([
      imageHash,
      undefined,
      '(235, 235, 235)',
    ]);

    // Expect that Minio's putObject method has been called 4 times to store the image.
    expect(mockPutObject).toHaveBeenCalledTimes(4);

    expect(response.statusCode).toBe(200);
  });

  it('should process uploaded vertical GIF files', async () => {
    const imageHash = '93001300000';

    mockPgQuery.mockReturnValueOnce({
      rows: [],
    });

    mockPgQuery.mockReturnValueOnce({
      rows: [{ image_id: 'exists' }],
    });
    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer no_exists')
      .attach(
        'image',
        path.join(__dirname, '__fixtures__', 'test_vertical.gif')
      );

    expect(mockPgQuery).toHaveBeenCalledTimes(2);
    expect(mockPgQuery.mock.calls[0][1]).toEqual([imageHash]);
    expect(mockPgQuery.mock.calls[1][1]).toEqual([
      imageHash,
      undefined,
      '(235, 235, 235)',
    ]);

    // Expect that Minio's putObject method has been called 4 times to store the image.
    expect(mockPutObject).toHaveBeenCalledTimes(4);

    expect(response.statusCode).toBe(200);
  });

  it('should not re-upload already uploaded files', async () => {
    mockPgQuery.mockReturnValueOnce({
      rows: [{ image_id: 'already_exists' }],
    });
    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer no_exists')
      .attach(
        'image',
        path.join(__dirname, '__fixtures__', 'test_horizontal.jpg')
      );

    expect(mockPgQuery).toHaveBeenCalledTimes(1);

    // Expect that Minio hasn't stored any files.
    expect(mockPutObject).toHaveBeenCalledTimes(0);

    expect(response.statusCode).toBe(200);
  });

  it('should not process uploaded SVG files', async () => {
    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer no_exists')
      .attach(
        'image',
        path.join(__dirname, '__fixtures__', 'test_horizontal.svg')
      );

    expect(mockPgQuery).toHaveBeenCalledTimes(0);
    expect(mockPutObject).toHaveBeenCalledTimes(0);

    expect(response.statusCode).toBe(400);
  });

  it('should not process PSD files', async () => {
    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer no_exists')
      .attach('image', path.join(__dirname, '__fixtures__', 'bad_format.psd'));

    expect(mockPgQuery).toHaveBeenCalledTimes(0);
    expect(mockPutObject).toHaveBeenCalledTimes(0);

    expect(response.statusCode).toBe(400);
  });

  it('should not process erroneous files', async () => {
    const response = await request(app)
      .post(IMAGE_UPLOAD_ENDPOINT)
      .set('authorization', 'Bearer no_exists')
      .attach('image', path.join(__dirname, '__fixtures__', 'text_format.txt'));

    expect(mockPgQuery).toHaveBeenCalledTimes(0);
    expect(mockPutObject).toHaveBeenCalledTimes(0);

    expect(response.statusCode).toBe(400);
  });
});
