const mockPgConnect = jest.fn();
const mockPgQuery = jest.fn();
const mockPgEnd = jest.fn();

const client = {
  connect: mockPgConnect,
  query: mockPgQuery,
  end: mockPgEnd,
};

module.exports = { Client: jest.fn(() => client) };
module.exports.mockPgConnect = mockPgConnect;
module.exports.mockPgQuery = mockPgQuery;
module.exports.mockPgEnd = mockPgEnd;
