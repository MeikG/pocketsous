const mockPutObject = jest.fn();

const client = {
  putObject: mockPutObject,
};

module.exports = { Client: jest.fn(() => client) };
module.exports.mockPutObject = mockPutObject;
