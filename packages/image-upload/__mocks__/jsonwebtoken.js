const mockJwtVerify = jest.fn();

module.exports = { verify: mockJwtVerify };
module.exports.mockJwtVerify = mockJwtVerify;
