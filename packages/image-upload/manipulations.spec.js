const Jimp = require('jimp');
const ColorThief = require('color-thief-jimp');
const { mockPgQuery } = require('pg');
const { mockPutObject } = require('minio');
const { getHash, getImageVariants } = require('./manipulations');

jest.mock('jimp', () => {
  const read = {
    clone: jest.fn().mockReturnThis(),
    quality: jest.fn().mockReturnThis(),
    cover: jest.fn().mockReturnThis(),
    getBufferAsync: jest.fn(async () => 'buffer'),
  };

  return {
    read: jest.fn().mockImplementation(async buffer => ({
      hash: jest.fn(() =>
        buffer
          .split('')
          .reverse()
          .join('')
      ),
      ...read,
    })),
  };
});

jest.mock('color-thief-jimp', () => ({
  getColor: jest.fn(() => 'dominant colour'),
}));

beforeEach(() => {
  jest.clearAllMocks();
  mockPgQuery.mockReset();
  mockPutObject.mockReset();
});


describe('getHash', () => {
  it('reads the buffer and returns a hashed value', async () => {
    const hash = await getHash('foobar1234');

    // Expect that the read function has been called with the buffer value.
    expect(Jimp.read).toHaveBeenCalled();
    expect(Jimp.read).toHaveBeenCalledWith('foobar1234');

    // The hash value is a reversed string of the input.
    expect(hash).toEqual('4321raboof');
  });

  it('throws errors if Jimp fails to read the file', () => {
    const errorMessage = 'cannot read buffer';

    Jimp.read.mockImplementationOnce(() => {
      throw new Error(errorMessage);
    });

    return getHash('foobar1234')
      .then(() => {
        // If we've got this far, the test has failed.
        throw new Error('test failed due to no errors being caught!');
      })
      .catch(e => {
        expect(e).toEqual(new Error(errorMessage));
      });
  });

  it('throws an error if Jimp fails to generate a hash', () => {
    const errorMessage = 'cannot generate hash';
    Jimp.read.mockImplementationOnce(buffer => ({
      hash: jest.fn(() => {
        throw new Error(errorMessage);
      }),
    }));

    return getHash('foobar1234')
      .then(() => {
        // If we've got this far, the test has failed.
        throw new Error('test failed due to no errors being caught!');
      })
      .catch(e => {
        expect(e).toEqual(new Error(errorMessage));
      });
  });
});

describe('getImageVariants', () => {
  it('throws an error if Jimp fails to read the image', () => {
    const errorMessage = 'cannot read buffer';

    Jimp.read.mockImplementationOnce(() => {
      throw new Error(errorMessage);
    });

    return getImageVariants('foobar1234')
      .then(() => {
        // If we've got this far, the test has failed.
        throw new Error('test failed due to nom errors being caught!');
      })
      .catch(e => {
        expect(e).toEqual(new Error(errorMessage));
      });
  });

  it('throws an error if Jimp cannot clone an image', () => {
    const errorMessage = 'cannot clone image';

    Jimp.read.mockImplementationOnce(() => ({
      clone: jest.fn(() => {
        throw new Error(errorMessage);
      }),
    }));

    return getImageVariants('foobar1234')
      .then(() => {
        // If we've got this far, the test has failed.
        throw new Error('test failed due to no errors being caught!');
      })
      .catch(e => {
        expect(e).toEqual(new Error(errorMessage));
      });
  });

  it('throws an error if Jimp cannot set the requested quality', () => {
    const errorMessage = 'cannot set quality';

    const clone = jest.fn(() => jimpImplementation);
    const quality = jest.fn(() => {
      throw new Error(errorMessage);
    });
    const cover = jest.fn(() => jimpImplementation);
    const getBufferAsync = jest.fn(() => jimpImplementation);

    const jimpImplementation = {
      clone,
      quality,
      cover,
      getBufferAsync,
    };

    Jimp.read.mockImplementationOnce(() => jimpImplementation);

    return getImageVariants('foobar1234')
      .then(() => {
        // If we've got this far, the test has failed.
        throw new Error('test failed due to no errors being caught!');
      })
      .catch(e => {
        expect(e).toEqual(new Error(errorMessage));
        expect(getBufferAsync).not.toHaveBeenCalled();
      });
  });

  it('throws an error if Jimp cannot set the cover size', () => {
    const errorMessage = 'cannot set cover';

    const clone = jest.fn(() => jimpImplementation);
    const quality = jest.fn(() => jimpImplementation);
    const cover = jest.fn(() => {
      throw new Error(errorMessage);
    });
    const getBufferAsync = jest.fn(() => jimpImplementation);

    const jimpImplementation = {
      clone,
      quality,
      cover,
      getBufferAsync,
    };

    Jimp.read.mockImplementationOnce(() => jimpImplementation);

    return getImageVariants('foobar1234')
      .then(() => {
        // If we've got this far, the test has failed.
        throw new Error('test failed due to no errors being caught!');
      })
      .catch(e => {
        expect(e).toEqual(new Error(errorMessage));
        expect(getBufferAsync).not.toHaveBeenCalled();
      });
  });

  it('throws an error if Jimp cannot get the buffer value', () => {
    const errorMessage = 'cannot get buffer async';

    const clone = jest.fn(() => jimpImplementation);
    const quality = jest.fn(() => jimpImplementation);
    const cover = jest.fn(() => {
      throw new Error(errorMessage);
    });
    const getBufferAsync = jest.fn(async () => {
      throw new error(errorMessage);
    });

    const jimpImplementation = {
      clone,
      quality,
      cover,
      getBufferAsync,
    };

    Jimp.read.mockImplementationOnce(() => jimpImplementation);

    return getImageVariants('foobar1234')
      .then(() => {
        // If we've got this far, the test has failed.
        throw new Error('test failed due to no errors being caught!');
      })
      .catch(e => {
        expect(e).toEqual(new Error(errorMessage));
        expect(getBufferAsync).not.toHaveBeenCalled();
      });
  });

  it('throws an error if Color Thief fails', () => {
    const errorMessage = 'cannot get dominant colour';

    ColorThief.getColor.mockImplementationOnce(() => {
      throw new Error(errorMessage);
    });

    return getImageVariants('foobar1234')
      .then(() => {
        // If we've got this far, the test has failed.
        throw new Error('test failed due to no errors being caught!');
      })
      .catch(e => {
        expect(e).toEqual(new Error(errorMessage));
      });
  });

  it('converts images into their variants, and returns the dominant colour', async () => {
    const images = await getImageVariants('foobar1234');
    expect(images).toEqual({
      dominantColor: 'dominant colour',
      originalImage: 'buffer',
      portraitCard: 'buffer',
      thumbnail: 'buffer',
      titleCard: 'buffer',
    });
  });
});
