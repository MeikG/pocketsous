const Jimp = require('jimp');
const ColorThief = require('color-thief-jimp');

/**
 * Manipulate the image uploaded, and return versions/data of the image.
 * @param buffer
 */
async function getHash(buffer) {
  const image = await Jimp.read(buffer);
  return image.hash();
}
async function getImageVariants(buffer) {
  const image = await Jimp.read(buffer);

  const titleCardPromise = image
    .clone()
    .quality(90)
    .cover(768, 576)
    .getBufferAsync(Jimp.MIME_JPEG);

  const portraitCardPromise = image
    .clone()
    .quality(60)
    .cover(243, 432)
    .getBufferAsync(Jimp.MIME_JPEG);

  const thumbnailPromise = image
    .clone()
    .quality(60)
    .cover(128, 128)
    .getBufferAsync(Jimp.MIME_JPEG);

  const dominantColor = ColorThief.getColor(image);

  const [
    originalImage,
    titleCard,
    portraitCard,
    thumbnail,
  ] = await Promise.all([
    image.getBufferAsync(Jimp.MIME_JPEG),
    titleCardPromise,
    portraitCardPromise,
    thumbnailPromise,
  ]);

  return {
    originalImage,
    titleCard,
    portraitCard,
    thumbnail,
    dominantColor,
  };
}

module.exports = {
  getHash,
  getImageVariants,
};
