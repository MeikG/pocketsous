const express = require('express');
const multer = require('multer');
const Minio = require('minio');
const cors = require('cors');
const shortId = require('shortid');
const jwt = require('jsonwebtoken');
const { Client } = require('pg');
const { getHash, getImageVariants } = require('./manipulations');
const { version } = require('./package.json');

const {
  NODE_ENV,
  S3_ENDPOINT,
  S3_ACCESS_KEY,
  S3_SECRET_KEY,
  IMAGES_BUCKET,
  S3_EXTERNAL_URL,
  POCKETSOUS_SECRET,
  POCKETSOUS_AUDIENCE,
  POSTGRES_URI,
  IMAGE_UPLOAD_ORIGINAL_FILENAME,
  IMAGE_UPLOAD_TITLE_CARD_FILENAME,
  IMAGE_UPLOAD_PORTRAIT_CARD_FILENAME,
  IMAGE_UPLOAD_THUMBNAIL_FILENAME,
  IMAGE_UPLOAD_ENDPOINT,
  VERSION_ENDPOINT,
} = process.env;

const dev = NODE_ENV === 'development';

const minioClient = new Minio.Client({
  useSSL: !dev,
  endPoint: S3_ENDPOINT,
  accessKey: S3_ACCESS_KEY,
  secretKey: S3_SECRET_KEY,
  port: dev ? 9000 : undefined,
});

const pgClient = new Client({
  connectionString: POSTGRES_URI,
});

if (dev) {
  const s3BucketPolicy = {
    Version: '2012-10-17',
    Statement: [
      {
        Sid: 'AddPerm',
        Effect: 'Allow',
        Principal: '*',
        Action: ['s3:GetObject'],
        Resource: [`arn:aws:s3:::${IMAGES_BUCKET}/*`],
      },
    ],
  };

  minioClient
    .bucketExists(IMAGES_BUCKET)
    .then(async exists => {
      if (!exists) {
        await minioClient.makeBucket(IMAGES_BUCKET);
        await minioClient.setBucketPolicy(
          IMAGES_BUCKET,
          JSON.stringify(s3BucketPolicy)
        );
      }
    })
    .catch(console.error);
}

// SET STORAGE
const upload = multer({ storage: multer.memoryStorage() });

function mimetypeToExtension(mimetype) {
  switch (mimetype) {
    default:
      throw new Error('Unsupported file type');
    case 'image/jpeg':
      return 'jpeg';
    case 'image/png':
      return 'png';
    case 'image/bmp':
      return 'bmp';
  }
}

const app = express();
const uploadMiddleware = multer().single('image');

Promise.all([pgClient.connect()]).then(() => {
  app.post(IMAGE_UPLOAD_ENDPOINT, cors(), (req, res) => {
    uploadMiddleware(req, res, async function(err) {
      if (err instanceof multer.MulterError) {
        console.error(err);
        return res.status(400).send();
      } else if (err) {
        console.error(err);
        return res.status(500).send();
      }
      const { authorization } = req.headers;

      // If no authorisation header is set, return an unauthenticated user.
      if (!authorization || !/^Bearer /m.test(authorization)) {
        return res.status(401).send();
      }

      // Extract the ID Token from the authorization header.
      const token = authorization.replace(/^Bearer /m, '');

      try {
        jwt.verify(token, POCKETSOUS_SECRET, {
          algorithm: 'HS512',
          issuer: POCKETSOUS_AUDIENCE,
          audience: POCKETSOUS_AUDIENCE,
          clockTolerance: 300,
        });
      } catch (e) {
        console.error(e);
        return res.status(403).send();
      }

      let hash;
      try {
        hash = await getHash(req.file.buffer);
      } catch (e) {
        console.error(e);
        return res.status(400).send();
      }

      try {
        const {
          rows: [exists],
          rows: getRows,
        } = await pgClient.query(
          'SELECT image_id FROM image WHERE image_id = $1::text',
          [hash]
        );

        if (!exists) {
          const {
            originalImage,
            titleCard,
            portraitCard,
            thumbnail,
            dominantColor,
          } = await getImageVariants(req.file.buffer);

          const originalPromise = minioClient.putObject(
            IMAGES_BUCKET,
            `${hash}_${IMAGE_UPLOAD_ORIGINAL_FILENAME}.jpeg`,
            originalImage
          );

          const titleCardPromise = minioClient.putObject(
            IMAGES_BUCKET,
            `${hash}_${IMAGE_UPLOAD_TITLE_CARD_FILENAME}.jpeg`,
            titleCard
          );

          const portraitCardPromise = minioClient.putObject(
            IMAGES_BUCKET,
            `${hash}_${IMAGE_UPLOAD_PORTRAIT_CARD_FILENAME}.jpeg`,
            portraitCard
          );

          const thumbnailPromise = minioClient.putObject(
            IMAGES_BUCKET,
            `${hash}_${IMAGE_UPLOAD_THUMBNAIL_FILENAME}.jpeg`,
            thumbnail
          );

          await Promise.all([
            originalPromise,
            titleCardPromise,
            portraitCardPromise,
            thumbnailPromise,
          ]);

          const {
            rows: [query],
            rows: secondRows,
          } = await pgClient.query(
            'INSERT INTO public.image(image_id, bucket, dominant_colour) ' +
              'VALUES ($1::text, $2::text, $3::COLOUR) ' +
              'RETURNING image_id',
            [
              hash,
              S3_EXTERNAL_URL,
              `(${dominantColor[0]}, ${dominantColor[1]}, ${dominantColor[2]})`,
            ]
          );

          return res.json({ id: query.image_id });
        }

        return res.json({ id: exists.image_id });
      } catch (e) {
        console.error(e);
        return res.status(500).send();
      }
    });
  });

  app.get(VERSION_ENDPOINT, (req, res) => {
    return res.status(200).send({ version });
  });

  app.emit('serverReady');
});

module.exports = app;
