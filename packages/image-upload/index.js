const app = require('./app');

const server = app.listen(8080, () =>
  console.log(`Image Upload listening on port 8080!`)
);

process.on('SIGTERM', () => {
  pgClient.end();
  server.close(() => {
    console.log('Process terminated');
  });
});
