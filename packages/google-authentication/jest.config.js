module.exports = {
  testEnvironment: 'node',
  coveragePathIgnorePatterns: ['/node_modules/'],
  setupFiles: ['<rootDir>/jest.setup.js'],
  testTimeout: 3600000,
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: '../../',
        classNameTemplate: '{classname}',
        titleTemplate: '{title}',
        uniqueOutputName: 'true',
        ancestorSeparator: ' › ',
      },
    ],
  ],
};
