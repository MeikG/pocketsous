const jwt = require('jsonwebtoken');

const { AUTHENTICATION_SECRET, AUTHENTICATION_AUDIENCE } = process.env;

// Exchange a Google ID token for a Postgraphile ID token.
module.exports = async req => {
  const { authorization } = req.headers;

  // If no authorisation header is set, return an unauthenticated user.
  if (!authorization) {
    return {
      role: 'anonymous',
    };
  }

  if (!/^Bearer /m.test(authorization)) {
    throw new Error('Invalid authorization type');
  }

  // Extract the ID Token from the authorization header.
  const token = authorization.replace(/^Bearer /m, '');

  return jwt.verify(
    token,
    AUTHENTICATION_SECRET,
    {
      algorithm: 'HS512',
      issuer: AUTHENTICATION_AUDIENCE,
      audience: AUTHENTICATION_AUDIENCE,
      clockTolerance: 300,
    },
    (err, claims) => {
      if (err) throw new Error('Invalid authorization token');

      return {
        'user.user_id': claims.sub,
        role: claims.role,
      };
    }
  );
};
