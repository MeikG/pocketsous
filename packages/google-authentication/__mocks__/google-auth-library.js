const verifyIdToken = jest.fn(({ idToken }) => ({
  getPayload: () => ({
    sub: idToken,
    name: 'bar',
    picture: 'bazz',
    email: 'foo@bar.test',
    email_verified: true,
    iat: 123,
    exp: 456,
  }),
}));

module.exports = { OAuth2Client: jest.fn(() => ({ verifyIdToken })) };
module.exports.mockVerifyIdToken = verifyIdToken;
