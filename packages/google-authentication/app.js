const express = require('express');
const jwt = require('jsonwebtoken');
const { Client: PgClient } = require('pg');
const cors = require('cors');
const { OAuth2Client } = require('google-auth-library');
const { version } = require('./package.json');

const {
  NODE_ENV,
  POCKETSOUS_SECRET,
  POCKETSOUS_AUDIENCE,
  POSTGRES_URI,
  GOOGLE_OAUTH2_WEB_CLIENT_ID,
  GOOGLE_OAUTH2_ANDROID_CLIENT_ID,
  GOOGLE_OAUTH2_IOS_CLIENT_ID,
  GOOGLE_OAUTH2_WEB_SECRET,
  EXCHANGE_ENDPOINT,
  EXCHANGE_CALLBACK_ENDPOINT,
  VERSION_ENDPOINT,
} = process.env;

const dev = NODE_ENV === 'development';

const client = new OAuth2Client();

const pgClient = new PgClient({
  connectionString: POSTGRES_URI,
});

pgClient.connect();

const app = express();

// Exchange a Google ID token for a Postgraphile ID token.
app.post(EXCHANGE_ENDPOINT, cors(), async (req, res) => {
  const { token: idToken } = req.query;

  // If no authorisation header is set, return an unauthenticated user.
  if (!idToken) {
    return res.status(400).send();
  }

  try {
    const ticket = await client.verifyIdToken({
      idToken,
      audience: [
        GOOGLE_OAUTH2_WEB_CLIENT_ID,
        GOOGLE_OAUTH2_ANDROID_CLIENT_ID,
        GOOGLE_OAUTH2_IOS_CLIENT_ID,
      ],
      maxExpiry: 0,
    });

    const payload = ticket.getPayload();

    const {
      rows: [{ user_id: userId, role }],
    } = await pgClient.query(
      `SELECT user_id, role FROM identity_federation.login_google($1::text, $2::text, $3::text, $4::text)`,
      [
        payload.sub,
        payload.name,
        payload.picture,
        payload.email_verified ? payload.email : null,
      ]
    );

    if (!userId || !role) return res.status(500).send();

    return jwt.sign(
      {
        sub: userId,
        role,
        iat: payload.iat,
        exp: payload.exp,
        method: 'google',
      },
      POCKETSOUS_SECRET,
      {
        algorithm: 'HS512',
        issuer: POCKETSOUS_AUDIENCE,
        audience: POCKETSOUS_AUDIENCE,
      },
      (err, token) => {
        if (err) {
          console.error(err);
          return res.status(500).send();
        }

        return res.send(token);
      }
    );
  } catch (e) {
    console.error(e);
    return res.status(401).send();
  }
});

if (dev) {
  const passport = require('passport');
  const GoogleStrategy = require('passport-google-oauth20').Strategy;

  app.use(passport.initialize());

  passport.use(
    new GoogleStrategy(
      {
        clientID: GOOGLE_OAUTH2_WEB_CLIENT_ID,
        clientSecret: GOOGLE_OAUTH2_WEB_SECRET,
        callbackURL: EXCHANGE_CALLBACK_ENDPOINT,
      },
      function(accessToken, refreshToken, tokens, profile, cb) {
        return cb(null, tokens.id_token);
      }
    )
  );

  app.get(
    EXCHANGE_ENDPOINT,
    passport.authenticate('google', { scope: ['profile', 'email'] })
  );

  app.get(
    EXCHANGE_CALLBACK_ENDPOINT,
    passport.authenticate('google', {
      failureRedirect: EXCHANGE_ENDPOINT,
      session: false,
    }),
    function(req, res) {
      return res.send(req.user);
    }
  );
}

app.get(VERSION_ENDPOINT, (req, res) => {
  return res.status(200).send({ version });
});

module.exports = app;
