# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.3.1](https://gitlab.com/MeikG/pocketsous/compare/v1.3.0...v1.3.1) (2020-05-14)

**Note:** Version bump only for package @pocketsous/google-authentication





# [1.3.0](https://gitlab.com/MeikG/pocketsous/compare/v1.1.0...v1.3.0) (2020-05-13)

**Note:** Version bump only for package @pocketsous/google-authentication





# [1.2.0](https://gitlab.com/MeikG/pocketsous/compare/v1.1.0...v1.2.0) (2020-05-13)

**Note:** Version bump only for package @pocketsous/google-authentication





# 1.1.0 (2020-04-14)


### Features

* add PATH_PREFIX to microservices to allow them to run on dynamic paths ([c82fb04](https://gitlab.com/MeikG/pocketsous/commit/c82fb04ffa57a0868c83f26c86014cd27cf34a0c))
