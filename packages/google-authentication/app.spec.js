const request = require('supertest');
const app = require('./app');
const { version } = require('./package.json');
const { mockPgQuery } = require('pg');
const { mockVerifyIdToken } = require('google-auth-library');
const jwt = require('jsonwebtoken');

jest.mock('jsonwebtoken', () => {
  return {
    sign: jest.fn().mockImplementationOnce((payload, secret, options, cb) => {
      return cb(null, JSON.stringify({ ...payload, secret, ...options }));
    }),
  };
});

jest.mock('./package.json', () => ({
  version: 'foo',
}));

const { EXCHANGE_ENDPOINT, VERSION_ENDPOINT } = process.env;

beforeEach(() => {
  jest.clearAllMocks();
});

describe('Google Authentication Endpoint', () => {
  it("should reject requests which don't specify a token", async () => {
    const response = await request(app).post(EXCHANGE_ENDPOINT);

    expect(response.statusCode).toBe(400);

    // The JWT should not have even attempted to be verified.
    expect(mockVerifyIdToken).not.toBeCalled();
  });

  it('should reject with an invalid token', async () => {
    mockVerifyIdToken.mockImplementationOnce(({ idToken }) => ({
      getPayload: () => {
        throw new Error('error');
      },
    }));

    const response = await request(app).post(`${EXCHANGE_ENDPOINT}?token=fail`);

    // Expect that the Google OAuth verify command has been executed.
    expect(mockVerifyIdToken).toBeCalledTimes(1);

    // The query should not have made it to the database.
    expect(mockPgQuery).not.toBeCalled();

    expect(response.statusCode).toBe(401);
  });

  it('should reject if the database does not come back with a user_id', async () => {
    mockPgQuery.mockImplementationOnce(() => ({
      rows: [{ role: 'bar' }],
    }));

    const response = await request(app).post(
      `${EXCHANGE_ENDPOINT}?token=no_user_id`
    );

    // The JWT sign method shouldn't be called if an invalid request has come back.
    expect(jwt.sign).not.toBeCalled();

    expect(response.statusCode).toBe(500);
  });

  it('should reject if the database does not come back with a role', async () => {
    mockPgQuery.mockImplementationOnce(() => ({
      rows: [{ user_id: 'bar' }],
    }));

    const response = await request(app).post(
      `${EXCHANGE_ENDPOINT}?token=no_role`
    );

    // The JWT sign method shouldn't be called if an invalid request has come back.
    expect(jwt.sign).not.toBeCalled();

    expect(response.statusCode).toBe(500);
  });

  it('should send a 500 if JWT fails to sign', async () => {
    jwt.sign.mockImplementationOnce((payload, secret, options, cb) => {
      return cb('error');
    });

    mockPgQuery.mockImplementationOnce(() => ({
      rows: [{ user_id: 'bar' }],
    }));

    const response = await request(app).post(
      `${EXCHANGE_ENDPOINT}?token=fail_jwt`
    );

    expect(response.statusCode).toBe(500);
  });

  it('should accept a valid token', async () => {
    mockPgQuery.mockImplementationOnce(() => ({
      rows: [{ user_id: 'foo', role: 'bar' }],
    }));

    const response = await request(app).post(
      `${EXCHANGE_ENDPOINT}?token=email`
    );

    expect(response.statusCode).toBe(200);
    expect(response.text).toBe(
      '{"sub":"foo","role":"bar","iat":123,"exp":456,"method":"google","secret":"testing","algorithm":"HS512","issuer":"jest","audience":"jest"}'
    );
  });
});

describe('Version Endpoint', () => {
  it('should return the current version', async () => {
    const response = await request(app).get(VERSION_ENDPOINT);

    expect(response.statusCode).toBe(200);

    expect(response.body).toEqual({
      version: 'foo',
    });
  });
});
