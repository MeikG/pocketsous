DROP USER IF EXISTS ${image_upload_user};

CREATE ROLE ${image_upload_user} WITH
	LOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	NOINHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD '${image_upload_password}';

CREATE TYPE public.COLOUR AS
(
	red smallint,
	green smallint,
	blue smallint
);

CREATE TABLE image
(
    image_id TEXT NOT NULL PRIMARY KEY,
    date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    bucket CHARACTER VARYING (255) NOT NULL,
    dominant_colour COLOUR NOT NULL
)
WITH (
    OIDS = FALSE
);

ALTER TABLE recipe OWNER to root;
GRANT SELECT ON TABLE public.image TO anonymous;
GRANT SELECT, INSERT ON TABLE public.image TO image_upload;

COMMENT ON TABLE public.image IS '@omit create,update,delete,all';
COMMENT ON COLUMN public.image.image_id IS E'@omit create,update';
COMMENT ON COLUMN public.image.bucket IS E'@omit';

CREATE FUNCTION image_original(image public.image) RETURNS text AS $$
  SELECT image.bucket || '/' || image.image_id || '_${image_upload_original_filename}.jpeg'
$$ LANGUAGE sql STABLE;

CREATE FUNCTION image_title_card(image public.image) RETURNS text AS $$
  SELECT image.bucket || '/' || image.image_id || '_${image_upload_title_card_filename}.jpeg'
$$ LANGUAGE sql STABLE;

CREATE FUNCTION image_portrait_card(image public.image) RETURNS text AS $$
  SELECT image.bucket || '/' || image.image_id || '_${image_upload_portrait_card_filename}.jpeg'
$$ LANGUAGE sql STABLE;

CREATE FUNCTION image_thumbnail(image public.image) RETURNS text AS $$
  SELECT image.bucket || '/' || image.image_id || '_${image_upload_thumbnail_filename}.jpeg'
$$ LANGUAGE sql STABLE;

CREATE TABLE recipe_image
(
    recipe_id UUID NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    image_id TEXT NOT NULL REFERENCES image ON DELETE CASCADE ON UPDATE CASCADE,
    date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    ordering SMALLINT NOT NULL,
    user_id UUID DEFAULT get_id() NOT NULL REFERENCES registered_user ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (recipe_id, image_id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE recipe_image OWNER to root;
ALTER TABLE recipe_image ENABLE ROW LEVEL SECURITY;
COMMENT ON TABLE public.recipe_image IS '@omit update,all';
COMMENT ON COLUMN public.recipe_image.recipe_id IS '@omit update';
COMMENT ON COLUMN public.recipe_image.date_created IS '@omit create,update';
COMMENT ON COLUMN public.recipe_image.image_id IS '@omit update';
COMMENT ON COLUMN public.recipe_image.user_id IS '@omit create,update';
COMMENT ON CONSTRAINT recipe_image_user_id_fkey ON public.recipe_image IS E'@foreignFieldName recipeImages\n@fieldName user';
COMMENT ON CONSTRAINT recipe_image_pkey ON public.recipe_image IS '@omit';

GRANT SELECT ON TABLE recipe_image TO anonymous;
GRANT INSERT(recipe_id, image_id, ordering), UPDATE(ordering), DELETE ON public.recipe_image TO authenticated;

CREATE POLICY recipe_image_select_authenticated_policy ON recipe_image FOR SELECT TO authenticated USING (user_id = get_id());
CREATE POLICY recipe_image_insert_authenticated_policy ON recipe_image FOR INSERT TO authenticated WITH CHECK (
  user_id = get_id() AND EXISTS(SELECT 1 FROM recipe WHERE recipe.recipe_id = recipe_id AND recipe.user_id = get_id())
);
CREATE POLICY recipe_image_update_authenticated_policy ON recipe_image FOR UPDATE TO authenticated USING (user_id = get_id());
CREATE POLICY recipe_image_delete_authenticated_policy ON recipe_image FOR DELETE TO authenticated USING (user_id = get_id());

COMMENT ON CONSTRAINT recipe_image_recipe_id_fkey ON recipe_image IS E'@foreignFieldName images\n@fieldName recipe';

CREATE INDEX ON "public"."recipe_image"("image_id");
CREATE INDEX ON "public"."recipe_image"("user_id");

CREATE TABLE ingredient_image
(
    ingredient_id UUID NOT NULL REFERENCES ingredient ON DELETE CASCADE ON UPDATE CASCADE PRIMARY KEY,
    image_id TEXT NOT NULL REFERENCES image ON DELETE CASCADE ON UPDATE CASCADE,
    date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    user_id UUID DEFAULT get_id() REFERENCES registered_user ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (
    OIDS = FALSE
);

ALTER TABLE ingredient_image OWNER to root;
ALTER TABLE ingredient_image ENABLE ROW LEVEL SECURITY;
COMMENT ON TABLE public.ingredient_image IS '@omit update,all';
COMMENT ON COLUMN public.ingredient_image.date_created IS '@omit create';
COMMENT ON COLUMN public.ingredient_image.user_id IS '@omit create';
COMMENT ON CONSTRAINT ingredient_image_user_id_fkey ON public.ingredient_image IS '@omit';
COMMENT ON CONSTRAINT ingredient_image_pkey ON public.ingredient_image IS '@omit';
COMMENT ON CONSTRAINT ingredient_image_ingredient_id_fkey ON public.ingredient_image IS E'@foreignFieldName image\n@fieldName ingredient\n@notNull';

GRANT SELECT ON TABLE ingredient_image TO anonymous;
GRANT INSERT(ingredient_id, image_id), DELETE ON public.ingredient_image TO authenticated;

CREATE POLICY ingredient_image_select_authenticated_policy ON ingredient_image FOR SELECT TO authenticated USING (user_id = get_id() OR user_id IS NULL);
CREATE POLICY ingredient_image_insert_authenticated_policy ON ingredient_image FOR INSERT TO authenticated WITH CHECK (
  user_id = get_id() AND EXISTS(SELECT 1 FROM ingredient WHERE ingredient.ingredient_id = ingredient_id AND ingredient.user_id = get_id())
);
CREATE POLICY ingredient_image_update_authenticated_policy ON ingredient_image FOR UPDATE TO authenticated USING (user_id = get_id());
CREATE POLICY ingredient_image_delete_authenticated_policy ON ingredient_image FOR DELETE TO authenticated USING (user_id = get_id());

CREATE INDEX ON public.ingredient_image("image_id");
CREATE INDEX ON public.ingredient_image("user_id");
