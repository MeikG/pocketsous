DROP USER IF EXISTS anonymous;
DROP USER IF EXISTS authenticated;
DROP USER IF EXISTS admin;
DROP USER IF EXISTS recipe_creator;

CREATE USER anonymous WITH
	NOLOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	INHERIT
	NOREPLICATION
	NOBYPASSRLS
	CONNECTION LIMIT -1;

CREATE USER authenticated WITH
	NOLOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	INHERIT
	NOREPLICATION
	NOBYPASSRLS
	CONNECTION LIMIT -1;

GRANT anonymous TO authenticated;

CREATE USER admin WITH
	NOLOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	INHERIT
	NOREPLICATION
	NOBYPASSRLS
	CONNECTION LIMIT -1;

GRANT authenticated TO admin;

CREATE USER recipe_creator WITH
	NOLOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	INHERIT
	NOREPLICATION
	NOBYPASSRLS
	CONNECTION LIMIT -1;

GRANT authenticated TO recipe_creator;

create extension if not exists "uuid-ossp";

CREATE TYPE role AS ENUM ('anonymous', 'authenticated', 'admin');

DROP OWNED BY anonymous CASCADE;
DROP OWNED BY authenticated CASCADE;
DROP OWNED BY admin CASCADE;

create function get_id() returns uuid as $$
  select nullif(current_setting('jwt.claims.sub', true), '')::uuid;
$$ language sql stable parallel safe;
COMMENT ON FUNCTION public.get_id() IS '@omit';

CREATE TABLE registered_user
(
    user_id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    photo text,
    display_name text NOT NULL,
    role role DEFAULT 'authenticated' NOT NULL
)
WITH (
    OIDS = FALSE
);

ALTER TABLE registered_user OWNER to root;

GRANT SELECT ON TABLE registered_user TO anonymous;
GRANT UPDATE ON TABLE registered_user TO authenticated;

ALTER TABLE registered_user ENABLE ROW LEVEL SECURITY;
CREATE POLICY user_select_policy ON registered_user FOR SELECT TO authenticated USING (true);
CREATE POLICY user_update_policy ON registered_user FOR UPDATE TO authenticated USING (user_id = get_id());

COMMENT ON TABLE registered_user IS '@omit create,update,delete,all';
COMMENT ON CONSTRAINT registered_user_pkey ON public.registered_user IS '@omit';

CREATE TABLE user_preference
(
    user_id uuid PRIMARY KEY REFERENCES registered_user ON DELETE CASCADE ON UPDATE CASCADE,
    date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    first_time boolean NOT NULL DEFAULT TRUE,
    marketing boolean NOT NULL DEFAULT FALSE,
    marketing_accepted date DEFAULT NULL
)
WITH (
    OIDS = FALSE
);

ALTER TABLE user_preference OWNER to root;

GRANT SELECT, UPDATE ON TABLE user_preference TO authenticated;

ALTER TABLE user_preference ENABLE ROW LEVEL SECURITY;
CREATE POLICY preference_authenticated_select_policy ON user_preference FOR SELECT TO authenticated USING (user_id = get_id());
CREATE POLICY preference_authenticated_update_policy ON user_preference FOR UPDATE TO authenticated USING (user_id = get_id());
CREATE POLICY preference_admin_select_policy ON user_preference FOR SELECT TO admin USING (true);
CREATE POLICY preference_admin_update_policy ON user_preference FOR UPDATE TO admin USING (true);

COMMENT ON TABLE user_preference IS '@omit create,delete,all';
COMMENT ON CONSTRAINT user_preference_pkey ON public.user_preference IS '@omit';
COMMENT ON CONSTRAINT user_preference_user_id_fkey ON user_preference IS E'@foreignFieldName userPreference\n@fieldName user';

CREATE FUNCTION get_current_user() RETURNS registered_user as $$
  SELECT * FROM public.registered_user WHERE user_id = get_id() LIMIT 1;
$$ LANGUAGE 'sql' stable parallel safe;

create type public.jwt_token as (
  role text,
  iat integer,
  exp integer,
  method text,
  aud text,
  iss text
);
