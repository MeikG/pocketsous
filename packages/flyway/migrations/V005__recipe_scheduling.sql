CREATE TYPE public.RECIPE_SCHEDULE_STATUS AS ENUM ('UNKNOWN', 'EATEN', 'SKIPPED');

CREATE TABLE recipe_schedule
(
    recipe_schedule_id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    recipe_id UUID NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    user_id UUID DEFAULT get_id() NOT NULL REFERENCES registered_user ON DELETE CASCADE ON UPDATE CASCADE,
    schedule_date TIMESTAMP WITH TIME ZONE NOT NULL,
    status RECIPE_SCHEDULE_STATUS DEFAULT 'UNKNOWN' NOT NULL,
    UNIQUE (recipe_id, schedule_date, user_id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE recipe_schedule OWNER to root;
COMMENT ON COLUMN public.recipe_schedule.recipe_schedule_id IS E'@omit create,update';
COMMENT ON COLUMN public.recipe_schedule.date_created IS '@omit create,update';
COMMENT ON COLUMN public.recipe_schedule.user_id IS '@omit create,update';
COMMENT ON COLUMN public.recipe_schedule.recipe_id IS '@omit update';
COMMENT ON COLUMN public.recipe_schedule.status IS '@omit create';
COMMENT ON CONSTRAINT recipe_schedule_user_id_fkey ON public.recipe_schedule IS E'@foreignFieldName recipeSchedules\n@fieldName user';
COMMENT ON CONSTRAINT recipe_schedule_recipe_id_schedule_date_user_id_key ON public.recipe_schedule IS '@omit';
CREATE INDEX ON "public"."recipe_schedule"("user_id");

GRANT SELECT, INSERT(recipe_id, schedule_date), UPDATE(schedule_date, status), DELETE ON public.recipe_schedule TO authenticated;

ALTER TABLE recipe_schedule ENABLE ROW LEVEL SECURITY;
CREATE POLICY recipe_schedule_select_authenticated_policy ON recipe_schedule FOR SELECT TO authenticated USING (user_id = get_id());
CREATE POLICY recipe_schedule_insert_authenticated_policy ON recipe_schedule FOR INSERT TO authenticated WITH CHECK (
  user_id = get_id() AND EXISTS(SELECT 1 FROM recipe WHERE recipe.recipe_id = recipe_id AND recipe.user_id = get_id())
);
CREATE POLICY recipe_schedule_update_authenticated_policy ON recipe_schedule FOR UPDATE TO authenticated USING (user_id = get_id());
CREATE POLICY recipe_schedule_delete_authenticated_policy ON recipe_schedule FOR DELETE TO authenticated USING (user_id = get_id());
