DROP SCHEMA IF EXISTS identity_federation CASCADE;
DROP USER IF EXISTS ${google_authentication_user};
DROP USER IF EXISTS google_authentication_execution;

-- Create a role to allow the google_authentication microservice to connect with.
CREATE ROLE ${google_authentication_user} WITH
	LOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	NOINHERIT
	NOREPLICATION
	NOBYPASSRLS
	CONNECTION LIMIT -1
	PASSWORD '${google_authentication_password}';

-- Create a role to execute the login_google function with.
CREATE ROLE google_authentication_execution WITH
	NOLOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	NOINHERIT
	NOREPLICATION
	NOBYPASSRLS
	CONNECTION LIMIT -1;

CREATE SCHEMA identity_federation
    AUTHORIZATION root;

GRANT USAGE ON SCHEMA identity_federation TO ${google_authentication_user};
GRANT USAGE ON SCHEMA identity_federation TO google_authentication_execution;

CREATE TABLE identity_federation.user_google_id
(
    user_google_id text PRIMARY KEY,
    user_id uuid NOT NULL REFERENCES registered_user ON DELETE CASCADE ON UPDATE CASCADE,
    date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    display_name text NOT NULL,
    photo text,
    email text
)
WITH (
    OIDS = FALSE
);

ALTER TABLE identity_federation.user_google_id OWNER to root;

-- Allow the execution context role permission to access the relevant tables.
GRANT INSERT, SELECT, UPDATE ON TABLE identity_federation.user_google_id TO google_authentication_execution;
GRANT INSERT, SELECT, UPDATE ON TABLE public.registered_user TO google_authentication_execution;
GRANT INSERT, SELECT, UPDATE ON TABLE public.user_preference TO google_authentication_execution;

-- Add policies to allow the execution role to access the data required to function.
CREATE POLICY registered_user_google_authentication_select_policy ON registered_user FOR SELECT TO google_authentication_execution USING (true);
CREATE POLICY registered_user_google_authentication_insert_policy ON registered_user FOR INSERT TO google_authentication_execution WITH CHECK (true);
CREATE POLICY user_preference_google_authentication_select_policy ON user_preference FOR SELECT TO google_authentication_execution USING (true);
CREATE POLICY user_preference_google_authentication_insert_policy ON user_preference FOR INSERT TO google_authentication_execution WITH CHECK (true);

CREATE FUNCTION identity_federation.login_google(IN in_google_id text, IN in_display_name text, IN in_photo text, IN in_email text)
    RETURNS registered_user
    LANGUAGE 'plpgsql'
    VOLATILE SECURITY DEFINER
AS $BODY$
DECLARE
	user_record registered_user;
BEGIN
	IF NOT EXISTS (SELECT FROM identity_federation.user_google_id WHERE user_google_id = in_google_id) THEN
		-- Create the user record.
		INSERT INTO public.registered_user(display_name, photo)
	    VALUES (in_display_name, in_photo)
    	RETURNING * INTO user_record;

    	-- Link the user record to the current Google ID.
		INSERT INTO identity_federation.user_google_id(user_google_id, user_id, display_name, photo, email)
	    VALUES (in_google_id, user_record.user_id, in_display_name, in_photo, in_email);

		-- Create the default user preferences.
		INSERT INTO public.user_preference(user_id)
	    VALUES (user_record.user_id);
	ELSE
		SELECT registered_user.*
		INTO user_record
		FROM identity_federation.user_google_id
		LEFT JOIN public.registered_user USING (user_id)
		WHERE user_google_id = in_google_id;

		-- Update the google record just in case some data has changed.
	    UPDATE identity_federation.user_google_id
    	SET email = in_email
    	WHERE user_id = user_record.user_id;
	END IF;
	RETURN user_record;
END
$BODY$;

ALTER FUNCTION identity_federation.login_google(text, text, text, text)
    OWNER TO google_authentication_execution;

-- Grant execution to the google_authentication microservice.
GRANT EXECUTE ON FUNCTION identity_federation.login_google(text, text, text, text) TO ${google_authentication_user};

COMMENT ON CONSTRAINT user_google_id_pkey ON identity_federation.user_google_id IS '@omit';

-- ALTER TABLE session ENABLE ROW LEVEL SECURITY;
-- DROP POLICY IF EXISTS user_session_policy ON "session";
-- CREATE POLICY user_session_policy ON "session" TO authenticated USING (user_id = get_id() AND valid = true);
