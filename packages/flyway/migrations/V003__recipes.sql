CREATE TYPE public.recipe_type AS ENUM ('MEAL', 'SIDE');
ALTER TYPE public.recipe_type OWNER TO root;

CREATE TYPE public.instruction_type AS ENUM ('MEAL_PREP', 'INSTRUCTION');
ALTER TYPE public.instruction_type OWNER TO root;

CREATE TABLE recipe
(
    recipe_id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    user_id UUID DEFAULT get_id() NOT NULL REFERENCES registered_user ON DELETE CASCADE ON UPDATE CASCADE,
    recipe_name TEXT NOT NULL,
    recipe_type RECIPE_TYPE NOT NULL,
    description TEXT NOT NULL DEFAULT '',
    UNIQUE (user_id, recipe_name)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE recipe OWNER to root;

COMMENT ON COLUMN public.recipe.recipe_id IS E'@omit create,update';
COMMENT ON COLUMN public.recipe.date_created IS '@omit create,update';
COMMENT ON COLUMN public.recipe.user_id IS '@omit create,update';
COMMENT ON CONSTRAINT recipe_user_id_fkey ON public.recipe IS E'@foreignFieldName recipes\n@fieldName user';
COMMENT ON CONSTRAINT recipe_user_id_recipe_name_key ON public.recipe IS '@omit';

GRANT SELECT ON TABLE recipe TO anonymous;
GRANT INSERT(recipe_name, recipe_type, description), UPDATE(recipe_name, recipe_type, description), DELETE ON public.recipe TO authenticated;

ALTER TABLE recipe ENABLE ROW LEVEL SECURITY;
CREATE POLICY recipe_select_authenticated_policy ON recipe FOR SELECT TO authenticated USING (user_id = get_id());
CREATE POLICY recipe_insert_authenticated_policy ON recipe FOR INSERT TO authenticated WITH CHECK (user_id = get_id());
CREATE POLICY recipe_update_authenticated_policy ON recipe FOR UPDATE TO authenticated USING (user_id = get_id());
CREATE POLICY recipe_delete_authenticated_policy ON recipe FOR DELETE TO authenticated USING (user_id = get_id());

CREATE INDEX ON public.recipe("user_id");

CREATE TABLE ingredient
(
    ingredient_id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    user_id UUID DEFAULT get_id() REFERENCES registered_user ON DELETE CASCADE ON UPDATE CASCADE,
    ingredient_name TEXT NOT NULL,
    UNIQUE (user_id, ingredient_name)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE ingredient OWNER to root;

CREATE INDEX ON public.ingredient("user_id");

COMMENT ON COLUMN public.ingredient.ingredient_id IS '@omit create,update';
COMMENT ON COLUMN public.ingredient.date_created IS '@omit create,update';
COMMENT ON COLUMN public.ingredient.user_id IS '@omit create,update';
COMMENT ON CONSTRAINT ingredient_user_id_fkey ON public.ingredient IS E'@foreignFieldName ingredients\n@fieldName user';
COMMENT ON CONSTRAINT ingredient_user_id_ingredient_name_key ON public.ingredient IS '@omit';

GRANT SELECT ON TABLE ingredient TO anonymous;
GRANT INSERT(ingredient_name), UPDATE(ingredient_name), DELETE ON public.ingredient TO authenticated;

ALTER TABLE ingredient ENABLE ROW LEVEL SECURITY;
CREATE POLICY ingredient_select_anonymous_policy ON ingredient FOR SELECT TO anonymous USING (user_id IS NULL);
CREATE POLICY ingredient_select_authenticated_policy ON ingredient FOR SELECT TO authenticated USING (user_id = get_id());
CREATE POLICY ingredient_insert_authenticated_policy ON ingredient FOR INSERT TO authenticated WITH CHECK (user_id = get_id());
CREATE POLICY ingredient_update_authenticated_policy ON ingredient FOR UPDATE TO authenticated USING (user_id = get_id());
CREATE POLICY ingredient_delete_authenticated_policy ON ingredient FOR DELETE TO authenticated USING (user_id = get_id());

CREATE TABLE recipe_ingredient
(
    recipe_id UUID NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    ingredient_id UUID NOT NULL REFERENCES ingredient ON DELETE CASCADE ON UPDATE CASCADE,
    date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    user_id UUID DEFAULT get_id() NOT NULL REFERENCES registered_user ON DELETE CASCADE ON UPDATE CASCADE,
    quantity TEXT NOT NULL,
    ordering SMALLINT NOT NULL,
    PRIMARY KEY (recipe_id, ingredient_id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE recipe_ingredient OWNER to root;

CREATE INDEX ON "public"."recipe_ingredient"("ingredient_id");
CREATE INDEX ON "public"."recipe_ingredient"("user_id");

COMMENT ON COLUMN public.recipe_ingredient.recipe_id IS '@omit update';
COMMENT ON COLUMN public.recipe_ingredient.date_created IS '@omit create,update';
COMMENT ON COLUMN public.recipe_ingredient.ingredient_id IS '@omit update';
COMMENT ON COLUMN public.recipe_ingredient.user_id IS '@omit';
COMMENT ON TABLE public.recipe_ingredient IS '@omit all';
COMMENT ON CONSTRAINT recipe_ingredient_recipe_id_fkey ON recipe_ingredient IS E'@foreignFieldName ingredients\n@fieldName recipe\n@notNull';
COMMENT ON CONSTRAINT recipe_ingredient_user_id_fkey ON public.recipe_ingredient IS '@omit';

GRANT SELECT, INSERT(recipe_id, ingredient_id, quantity, ordering), UPDATE(quantity, ordering), DELETE ON public.recipe_ingredient TO authenticated;

ALTER TABLE recipe_ingredient ENABLE ROW LEVEL SECURITY;
CREATE POLICY recipe_ingredient_select_authenticated_policy ON recipe_ingredient FOR SELECT TO authenticated USING (user_id = get_id());
CREATE POLICY recipe_ingredient_insert_authenticated_policy ON recipe_ingredient FOR INSERT TO authenticated WITH CHECK (
	user_id = get_id() AND EXISTS(SELECT 1 FROM recipe WHERE recipe.recipe_id = recipe_id AND recipe.user_id = get_id()) AND EXISTS(SELECT 1 FROM ingredient WHERE ingredient.ingredient_id = ingredient_id AND ingredient.user_id = get_id())
);
CREATE POLICY recipe_ingredient_update_authenticated_policy ON recipe_ingredient FOR UPDATE TO authenticated USING (user_id = get_id());
CREATE POLICY recipe_ingredient_delete_authenticated_policy ON recipe_ingredient FOR DELETE TO authenticated USING (user_id = get_id());

CREATE TABLE recipe_instruction
(
    recipe_instruction_id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    recipe_id UUID NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    date_created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    user_id UUID DEFAULT get_id() NOT NULL REFERENCES registered_user ON DELETE CASCADE ON UPDATE CASCADE,
    description TEXT NOT NULL,
    instruction_type INSTRUCTION_TYPE NOT NULL,
    ordering SMALLINT NOT NULL
)
WITH (
    OIDS = FALSE
);

ALTER TABLE recipe_instruction OWNER to root;
ALTER TABLE recipe_instruction ENABLE ROW LEVEL SECURITY;

COMMENT ON COLUMN public.recipe_instruction.recipe_instruction_id IS E'@omit create,update';
COMMENT ON COLUMN public.recipe_instruction.recipe_id IS '@omit update';
COMMENT ON CONSTRAINT recipe_instruction_pkey ON public.recipe_instruction IS '@omit all';
COMMENT ON COLUMN public.recipe_instruction.date_created IS '@omit create,update';
COMMENT ON COLUMN public.recipe_instruction.user_id IS '@omit create,update';
COMMENT ON CONSTRAINT recipe_instruction_recipe_id_fkey ON recipe_instruction IS E'@foreignFieldName instructions\n@fieldName recipe';
COMMENT ON CONSTRAINT recipe_instruction_user_id_fkey ON public.recipe_instruction IS '@omit';
COMMENT ON TABLE public.recipe_instruction IS '@omit all';

GRANT SELECT, INSERT(recipe_id, description, instruction_type, ordering), UPDATE(description, instruction_type, ordering), DELETE ON public.recipe_instruction TO authenticated;

ALTER TABLE recipe_ingredient ENABLE ROW LEVEL SECURITY;
CREATE POLICY recipe_instruction_select_authenticated_policy ON recipe_instruction FOR SELECT TO authenticated USING (user_id = get_id());
CREATE POLICY recipe_instruction_insert_authenticated_policy ON recipe_instruction FOR INSERT TO authenticated WITH CHECK (
  user_id = get_id() AND EXISTS(SELECT 1 FROM recipe WHERE recipe.recipe_id = recipe_id AND recipe.user_id = get_id())
);
CREATE POLICY recipe_instruction_update_authenticated_policy ON recipe_instruction FOR UPDATE TO authenticated USING (user_id = get_id());
CREATE POLICY recipe_instruction_delete_authenticated_policy ON recipe_instruction FOR DELETE TO authenticated USING (user_id = get_id());
CREATE INDEX ON "public"."recipe_instruction"("recipe_id");
CREATE INDEX ON "public"."recipe_instruction"("user_id");
