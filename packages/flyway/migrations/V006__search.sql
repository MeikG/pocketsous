ALTER TABLE public.recipe ADD COLUMN recipe_name_tsvector TSVECTOR;
ALTER TABLE public.recipe ADD COLUMN description_tsvector TSVECTOR;

COMMENT ON COLUMN public.recipe.recipe_name_tsvector IS '@omit';
COMMENT ON COLUMN public.recipe.description_tsvector IS '@omit';

CREATE FUNCTION public.recipe_tsvectors()
    RETURNS trigger
    LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
    SELECT to_tsvector(NEW.recipe_name) INTO NEW.recipe_name_tsvector;
    SELECT to_tsvector(NEW.description) INTO NEW.description_tsvector;
    RETURN NEW;
END
$BODY$;

CREATE TRIGGER recipe_tsvectors_update
  BEFORE UPDATE OF recipe_name, description
  ON public.recipe
  FOR EACH ROW
  EXECUTE PROCEDURE recipe_tsvectors();

CREATE TRIGGER recipe_tsvectors_insert
  BEFORE INSERT
  ON public.recipe
  FOR EACH ROW
  EXECUTE PROCEDURE recipe_tsvectors();

ALTER TABLE public.ingredient ADD COLUMN ingredient_name_tsvector TSVECTOR;

COMMENT ON COLUMN public.ingredient.ingredient_name_tsvector IS '@omit';

CREATE FUNCTION public.ingredient_tsvectors()
    RETURNS trigger
    LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
    SELECT to_tsvector(NEW.ingredient_name) INTO NEW.ingredient_name_tsvector;
    RETURN NEW;
END
$BODY$;

CREATE TRIGGER ingredient_tsvectors_update
  BEFORE UPDATE OF ingredient_name
  ON public.ingredient
  FOR EACH ROW
  EXECUTE PROCEDURE ingredient_tsvectors();

CREATE TRIGGER ingredient_tsvectors_insert
  BEFORE INSERT
  ON public.ingredient
  FOR EACH ROW
  EXECUTE PROCEDURE ingredient_tsvectors();

CREATE FUNCTION public.autocomplete_ingredients(IN text text)
    RETURNS SETOF ingredient
    LANGUAGE 'sql'
    STABLE STRICT PARALLEL SAFE
AS $BODY$
    SELECT ingredient.*
	FROM ingredient
	LEFT JOIN recipe_ingredient ON ingredient.ingredient_id = recipe_ingredient.ingredient_id
	WHERE ingredient_name LIKE concat('%', text, '%')
	GROUP BY ingredient.ingredient_id
	ORDER BY COUNT(recipe_ingredient.ingredient_id), ingredient_name
$BODY$;

ALTER FUNCTION public.autocomplete_ingredients(text)
    OWNER TO root;

GRANT EXECUTE ON FUNCTION public.autocomplete_ingredients(text) TO authenticated;

REVOKE ALL ON FUNCTION public.autocomplete_ingredients(text) FROM PUBLIC;
