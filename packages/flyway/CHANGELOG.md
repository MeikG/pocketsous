# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.3.1](https://gitlab.com/MeikG/pocketsous/compare/v1.3.0...v1.3.1) (2020-05-14)

**Note:** Version bump only for package @pocketsous/flyway





# [1.3.0](https://gitlab.com/MeikG/pocketsous/compare/v1.1.0...v1.3.0) (2020-05-13)

**Note:** Version bump only for package @pocketsous/flyway





# [1.2.0](https://gitlab.com/MeikG/pocketsous/compare/v1.1.0...v1.2.0) (2020-05-13)


### Bug Fixes

* **image_uploads:** update row level security ([d581779](https://gitlab.com/MeikG/pocketsous/commit/d581779050bba1f9823d2dbf16357b4523cd2d31))
* **recipes:** update row level security ([7d70319](https://gitlab.com/MeikG/pocketsous/commit/7d7031931ba371adffec7579308c39cc3ea27439))
* **search:** fix incorrect autocomplete function volatility ([a3385a0](https://gitlab.com/MeikG/pocketsous/commit/a3385a0f5ebc006d8a26e7547e6ec1e3a0ac44c2))


### Features

* **recipe_scheduling:** add initial recipe scheduling functionality ([718f909](https://gitlab.com/MeikG/pocketsous/commit/718f9099d60242b54b82f0cc14fba1969175fd9f))





# 1.1.0 (2020-04-14)

**Note:** Version bump only for package @pocketsous/flyway
