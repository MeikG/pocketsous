const { makeExtendSchemaPlugin, gql } = require('graphile-utils');
const { version } = require('./package.json');

module.exports = makeExtendSchemaPlugin(() => ({
  typeDefs: gql`
    extend type Query {
      version: String!
    }
  `,

  resolvers: {
    Query: {
      version: () => version,
    },
  },
}));
