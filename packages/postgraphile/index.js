const { postgraphile } = require('postgraphile');
const app = require('./app');
const configuration = require('./configuration');

const {
  POSTGRES_USER,
  POSTGRES_PASSWORD,
  POSTGRES_SERVER,
  POSTGRES_DB,
} = process.env;

app.use(
  postgraphile(
    `postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_SERVER}/${POSTGRES_DB}`,
    'public',
    configuration
  )
);

const server = app.listen(8080, () =>
  console.log(`Postgraphile listening on port 8080!`)
);

process.on('SIGTERM', () => {
  server.close(() => {
    console.log('Process terminated');
  });
});
