const request = require('supertest');
const app = require('../app');
const { version } = require('../package.json');

const { GRAPHQL_ENDPOINT } = process.env;

describe('Version', () => {
  it('returns the value as set in package.json', async () => {
    const response = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({ query: '{ version }' });

    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual({
      data: {
        version,
      },
    });
  });
});
