const request = require('supertest');
const randomstring = require('randomstring');
const app = require('../app');
const { userIds, generateAccessToken } = require('./token');

const { GRAPHQL_ENDPOINT } = process.env;

const createRecipeMutation = `
  mutation($recipeName: String!, $recipeType: RecipeType!, $description: String) {
    createRecipe(
      input: {
        recipe: {
          recipeName: $recipeName
          recipeType: $recipeType
          description: $description
        }
      }
    ) {
      recipe {
        recipeId
        recipeName
      }
    }
  }
`;

const createRecipeScheduleMutation = `
  mutation($recipeId: UUID!, $scheduleDate: Datetime!) {
    createRecipeSchedule(
      input: {
        recipeSchedule: { recipeId: $recipeId, scheduleDate: $scheduleDate }
      }
    ) {
      recipeSchedule {
        recipeScheduleId
        scheduleDate
        status
      }
    }
  }
`;

const recipeScheduleQuery = `
  query($recipeScheduleId: UUID!) {
    recipeSchedule(recipeScheduleId: $recipeScheduleId) {
      recipeScheduleId
      scheduleDate
      status
    }
  }
`;

const updateRecipeScheduleMutation = `
  mutation(
    $recipeScheduleId: UUID!
    $scheduleDate: Datetime
    $status: RecipeScheduleStatus
  ) {
    updateRecipeSchedule(
      input: {
        recipeScheduleId: $recipeScheduleId
        patch: { scheduleDate: $scheduleDate, status: $status }
      }
    ) {
      recipeSchedule {
        recipeScheduleId
        scheduleDate
        status
      }
    }
  }
`;

const deleteRecipeScheduleMutation = `
  mutation($recipeScheduleId: UUID!) {
    deleteRecipeSchedule(input: { recipeScheduleId: $recipeScheduleId }) {
      recipeSchedule {
        recipeScheduleId
        scheduleDate
        status
      }
    }
  }
`;

describe('Recipe Schedules', () => {
  describe('CRUD mutations', () => {
    it('can create schedules', async () => {
      const recipeName = randomstring.generate();

      const authenticatedToken = generateAccessToken({
        sub: userIds.authenticated1,
      });

      const {
        body: {
          data: {
            createRecipe: {
              recipe: { recipeId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeMutation,
          variables: {
            recipeName: recipeName,
            recipeType: 'MEAL',
          },
        });

      const {
        body: {
          data: {
            createRecipeSchedule: { recipeSchedule },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeScheduleMutation,
          variables: {
            recipeId,
            scheduleDate: '2020-01-01T00:00:00+00:00',
          },
        });

      expect(recipeSchedule).toEqual({
        recipeScheduleId: expect.anything(),
        scheduleDate: '2020-01-01T00:00:00+00:00',
        status: 'UNKNOWN',
      });
    });

    it('can update schedules ', async () => {
      const recipeName = randomstring.generate();

      const authenticatedToken = generateAccessToken({
        sub: userIds.authenticated1,
      });

      const {
        body: {
          data: {
            createRecipe: {
              recipe: { recipeId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeMutation,
          variables: {
            recipeName: recipeName,
            recipeType: 'MEAL',
          },
        });

      const {
        body: {
          data: {
            createRecipeSchedule: {
              recipeSchedule: { recipeScheduleId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeScheduleMutation,
          variables: {
            recipeId,
            scheduleDate: '2020-01-01T00:00:00+00:00',
          },
        });

      await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: updateRecipeScheduleMutation,
          variables: {
            recipeScheduleId,
            scheduleDate: '2021-01-01T00:00:00+00:00',
          },
        });

      const {
        body: {
          data: { recipeSchedule: updatedRecipeSchedule },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: recipeScheduleQuery,
          variables: {
            recipeScheduleId,
          },
        });

      expect(updatedRecipeSchedule).toEqual({
        recipeScheduleId: expect.anything(),
        scheduleDate: '2021-01-01T00:00:00+00:00',
        status: 'UNKNOWN',
      });
    });

    it('can read schedules', async () => {
      const recipeName = randomstring.generate();

      const authenticatedToken = generateAccessToken({
        sub: userIds.authenticated1,
      });

      const {
        body: {
          data: {
            createRecipe: {
              recipe: { recipeId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeMutation,
          variables: {
            recipeName: recipeName,
            recipeType: 'MEAL',
          },
        });

      const {
        body: {
          data: {
            createRecipeSchedule: {
              recipeSchedule: { recipeScheduleId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeScheduleMutation,
          variables: {
            recipeId,
            scheduleDate: '2020-01-01T00:00:00+00:00',
          },
        });

      const {
        body: {
          data: { recipeSchedule: readRecipeSchedule },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: recipeScheduleQuery,
          variables: {
            recipeScheduleId,
          },
        });

      expect(readRecipeSchedule).toEqual({
        recipeScheduleId: expect.anything(),
        scheduleDate: '2020-01-01T00:00:00+00:00',
        status: 'UNKNOWN',
      });
    });

    it('can delete schedules ', async () => {
      const recipeName = randomstring.generate();

      const authenticatedToken = generateAccessToken({
        sub: userIds.authenticated1,
      });

      const {
        body: {
          data: {
            createRecipe: {
              recipe: { recipeId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeMutation,
          variables: {
            recipeName: recipeName,
            recipeType: 'MEAL',
          },
        });

      const {
        body: {
          data: {
            createRecipeSchedule: {
              recipeSchedule: { recipeScheduleId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeScheduleMutation,
          variables: {
            recipeId,
            scheduleDate: '2020-01-01T00:00:00+00:00',
          },
        });

      await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: deleteRecipeScheduleMutation,
          variables: {
            recipeScheduleId,
          },
        });

      const {
        body: {
          data: { recipeSchedule: deletedRecipeSchedule },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: recipeScheduleQuery,
          variables: {
            recipeScheduleId,
          },
        });

      expect(deletedRecipeSchedule).toBeNull();
    });
  });

  describe('Authorisation', () => {
    it("prevents unauthorised users creating schedules on recipes they don't own", async () => {
      const recipeName = randomstring.generate();

      const authenticated1Token = generateAccessToken({
        sub: userIds.authenticated1,
      });

      const authenticated2Token = generateAccessToken({
        sub: userIds.authenticated2,
      });

      const {
        body: {
          data: {
            createRecipe: {
              recipe: { recipeId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated1Token}`)
        .send({
          query: createRecipeMutation,
          variables: {
            recipeName: recipeName,
            recipeType: 'MEAL',
          },
        });

      const {
        body: { errors },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated2Token}`)
        .send({
          query: createRecipeScheduleMutation,
          variables: {
            recipeId,
            scheduleDate: '2020-01-01T00:00:00+00:00',
          },
        });

      expect(errors[0].errcode).toEqual('42501');
    });

    it("prevents users from reading schedules they don't own", async () => {
      const recipeName = randomstring.generate();

      const authenticated1Token = generateAccessToken({
        sub: userIds.authenticated1,
      });

      const authenticated2Token = generateAccessToken({
        sub: userIds.authenticated2,
      });

      const {
        body: {
          data: {
            createRecipe: {
              recipe: { recipeId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated1Token}`)
        .send({
          query: createRecipeMutation,
          variables: {
            recipeName: recipeName,
            recipeType: 'MEAL',
          },
        });

      const {
        body: {
          data: {
            createRecipeSchedule: {
              recipeSchedule: { recipeScheduleId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated1Token}`)
        .send({
          query: createRecipeScheduleMutation,
          variables: {
            recipeId,
            scheduleDate: '2020-01-01T00:00:00+00:00',
          },
        });

      const {
        body: {
          data: { recipeSchedule },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated2Token}`)
        .send({
          query: recipeScheduleQuery,
          variables: {
            recipeScheduleId,
          },
        });

      expect(recipeSchedule).toBeNull();
    });

    it("prevents users updating schedules they don't own", async () => {
      const recipeName = randomstring.generate();

      const authenticated1Token = generateAccessToken({
        sub: userIds.authenticated1,
      });

      const authenticated2Token = generateAccessToken({
        sub: userIds.authenticated2,
      });

      const {
        body: {
          data: {
            createRecipe: {
              recipe: { recipeId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated1Token}`)
        .send({
          query: createRecipeMutation,
          variables: {
            recipeName: recipeName,
            recipeType: 'MEAL',
          },
        });

      const {
        body: {
          data: {
            createRecipeSchedule: {
              recipeSchedule: { recipeScheduleId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated1Token}`)
        .send({
          query: createRecipeScheduleMutation,
          variables: {
            recipeId,
            scheduleDate: '2020-01-01T00:00:00+00:00',
          },
        });

      const {
        body: { errors },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated2Token}`)
        .send({
          query: updateRecipeScheduleMutation,
          variables: {
            recipeScheduleId,
            scheduleDate: '2021-01-01T00:00:00+00:00',
          },
        });

      const {
        body: {
          data: { recipeSchedule: readRecipeSchedule },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated1Token}`)
        .send({
          query: recipeScheduleQuery,
          variables: {
            recipeScheduleId,
          },
        });

      expect(errors[0].message).toEqual(
        "No values were updated in collection 'recipe_schedules' because no values you can update were found matching these criteria."
      );

      expect(readRecipeSchedule).toEqual({
        recipeScheduleId: expect.anything(),
        scheduleDate: '2020-01-01T00:00:00+00:00',
        status: 'UNKNOWN',
      });
    });

    it('can delete schedules ', async () => {
      const recipeName = randomstring.generate();

      const authenticated1Token = generateAccessToken({
        sub: userIds.authenticated1,
      });

      const authenticated2Token = generateAccessToken({
        sub: userIds.authenticated2,
      });

      const {
        body: {
          data: {
            createRecipe: {
              recipe: { recipeId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated1Token}`)
        .send({
          query: createRecipeMutation,
          variables: {
            recipeName: recipeName,
            recipeType: 'MEAL',
          },
        });

      const {
        body: {
          data: {
            createRecipeSchedule: {
              recipeSchedule: { recipeScheduleId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated1Token}`)
        .send({
          query: createRecipeScheduleMutation,
          variables: {
            recipeId,
            scheduleDate: '2020-01-01T00:00:00+00:00',
          },
        });

      const {
        body: { errors },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated2Token}`)
        .send({
          query: deleteRecipeScheduleMutation,
          variables: {
            recipeScheduleId,
          },
        });

      const {
        body: {
          data: { recipeSchedule: deletedRecipeSchedule },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticated1Token}`)
        .send({
          query: recipeScheduleQuery,
          variables: {
            recipeScheduleId,
          },
        });

      expect(errors[0].message).toEqual(
        "No values were deleted in collection 'recipe_schedules' because no values you can delete were found matching these criteria."
      );

      expect(deletedRecipeSchedule).toEqual({
        recipeScheduleId: expect.anything(),
        scheduleDate: '2020-01-01T00:00:00+00:00',
        status: 'UNKNOWN',
      });
    });
  });

  describe('Anonymous users', () => {
    it("prevents anonymous users creating schedules on recipes they don't own", async () => {
      const recipeName = randomstring.generate();

      const authenticatedToken = generateAccessToken({
        sub: userIds.authenticated1,
      });

      const {
        body: {
          data: {
            createRecipe: {
              recipe: { recipeId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeMutation,
          variables: {
            recipeName: recipeName,
            recipeType: 'MEAL',
          },
        });

      const {
        body: { errors },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send({
          query: createRecipeScheduleMutation,
          variables: {
            recipeId,
            scheduleDate: '2020-01-01T00:00:00+00:00',
          },
        });

      expect(errors[0].errcode).toEqual('42501');
    });

    it('prevents anonymous users from reading schedules', async () => {
      const recipeName = randomstring.generate();

      const authenticatedToken = generateAccessToken({
        sub: userIds.authenticated1,
      });

      const {
        body: {
          data: {
            createRecipe: {
              recipe: { recipeId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeMutation,
          variables: {
            recipeName: recipeName,
            recipeType: 'MEAL',
          },
        });

      const {
        body: {
          data: {
            createRecipeSchedule: {
              recipeSchedule: { recipeScheduleId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeScheduleMutation,
          variables: {
            recipeId,
            scheduleDate: '2020-01-01T00:00:00+00:00',
          },
        });

      const {
        body: { errors },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send({
          query: recipeScheduleQuery,
          variables: {
            recipeScheduleId,
          },
        });

      expect(errors[0].errcode).toEqual('42501');
    });

    it('prevents anonymous users updating schedules', async () => {
      const recipeName = randomstring.generate();

      const authenticatedToken = generateAccessToken({
        sub: userIds.authenticated1,
      });

      const {
        body: {
          data: {
            createRecipe: {
              recipe: { recipeId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeMutation,
          variables: {
            recipeName: recipeName,
            recipeType: 'MEAL',
          },
        });

      const {
        body: {
          data: {
            createRecipeSchedule: {
              recipeSchedule: { recipeScheduleId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeScheduleMutation,
          variables: {
            recipeId,
            scheduleDate: '2020-01-01T00:00:00+00:00',
          },
        });

      const {
        body: { errors },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send({
          query: updateRecipeScheduleMutation,
          variables: {
            recipeScheduleId,
            scheduleDate: '2021-01-01T00:00:00+00:00',
          },
        });

      const {
        body: {
          data: { recipeSchedule: readRecipeSchedule },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: recipeScheduleQuery,
          variables: {
            recipeScheduleId,
          },
        });

      expect(errors[0].errcode).toEqual('42501');

      expect(readRecipeSchedule).toEqual({
        recipeScheduleId: expect.anything(),
        scheduleDate: '2020-01-01T00:00:00+00:00',
        status: 'UNKNOWN',
      });
    });

    it('prevents anonymous users from deleting schedules ', async () => {
      const recipeName = randomstring.generate();

      const authenticatedToken = generateAccessToken({
        sub: userIds.authenticated1,
      });

      const {
        body: {
          data: {
            createRecipe: {
              recipe: { recipeId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeMutation,
          variables: {
            recipeName: recipeName,
            recipeType: 'MEAL',
          },
        });

      const {
        body: {
          data: {
            createRecipeSchedule: {
              recipeSchedule: { recipeScheduleId },
            },
          },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: createRecipeScheduleMutation,
          variables: {
            recipeId,
            scheduleDate: '2020-01-01T00:00:00+00:00',
          },
        });

      const {
        body: { errors },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send({
          query: deleteRecipeScheduleMutation,
          variables: {
            recipeScheduleId,
          },
        });

      const {
        body: {
          data: { recipeSchedule: deletedRecipeSchedule },
        },
      } = await request(app)
        .post(GRAPHQL_ENDPOINT)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${authenticatedToken}`)
        .send({
          query: recipeScheduleQuery,
          variables: {
            recipeScheduleId,
          },
        });

      expect(errors[0].errcode).toEqual('42501');

      expect(deletedRecipeSchedule).toEqual({
        recipeScheduleId: expect.anything(),
        scheduleDate: '2020-01-01T00:00:00+00:00',
        status: 'UNKNOWN',
      });
    });
  });
});
