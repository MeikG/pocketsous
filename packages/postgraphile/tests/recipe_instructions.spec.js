const request = require('supertest');
const randomstring = require('randomstring');
const app = require('../app');
const { userIds, generateAccessToken } = require('./token');

const { GRAPHQL_ENDPOINT } = process.env;

const createRecipeMutation = `
  mutation($recipeName: String!, $recipeType: RecipeType!, $description: String) {
    createRecipe(
      input: {
        recipe: {
          recipeName: $recipeName
          recipeType: $recipeType
          description: $description
        }
      }
    ) {
      recipe {
        recipeId
        recipeName
      }
    }
  }
`;

const recipeQuery = `
  query($recipeId: UUID!) {
    recipe(recipeId: $recipeId) {
      recipeName
      ingredients {
        nodes {
          ingredient {
            ingredientId
          }
        }
      }
    }
  }
`;

const createRecipeInstructionMutation = `
  mutation(
    $recipeId: UUID!
    $description: String!
    $instructionType: InstructionType!
    $ordering: Int!
  ) {
    createRecipeInstruction(
      input: {
        recipeInstruction: {
          recipeId: $recipeId
          description: $description
          instructionType: $instructionType
          ordering: $ordering
        }
      }
    ) {
      recipeInstruction {
        recipeInstructionId
        description
        instructionType
        ordering
      }
    }
  }
`;

const recipeInstructionQuery = `
  query($recipeInstructionId: UUID!) {
    recipeInstruction(recipeInstructionId: $recipeInstructionId) {
      recipeInstructionId
      description
      instructionType
      ordering
    }
  }
`;

const updateRecipeInstructionMutation = `
  mutation(
    $recipeInstructionId: UUID!
    $description: String!
    $instructionType: InstructionType!
    $ordering: Int!
  ) {
    updateRecipeInstruction(
      input: {
        recipeInstructionId: $recipeInstructionId
        patch: {
          description: $description
          instructionType: $instructionType
          ordering: $ordering
        }
      }
    ) {
      recipeInstruction {
        recipeInstructionId
        description
        instructionType
        ordering
      }
    }
  }
`;

const deleteRecipeInstructionMutation = `
  mutation($recipeInstructionId: UUID!) {
    deleteRecipeInstruction(
      input: { recipeInstructionId: $recipeInstructionId }
    ) {
      recipeInstruction {
        recipeInstructionId
      }
    }
  }
`;

describe('Recipe Instructions', () => {
  it('allows authenticated users to create recipe instructions', async () => {
    const recipeName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: recipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName: recipeName,
          recipeType: 'MEAL',
        },
      });

    const { recipeId } = recipeBody.data.createRecipe.recipe;

    const { body: authenticated1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeInstructionMutation,
        variables: {
          recipeId,
          description: 'test_1',
          instructionType: 'MEAL_PREP',
          ordering: 0,
        },
      });

    const { body: authenticated2Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: createRecipeInstructionMutation,
        variables: {
          recipeId,
          description: 'test_2',
          instructionType: 'MEAL_PREP',
          ordering: 1,
        },
      });

    const { body: unauthenticatedBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: createRecipeInstructionMutation,
        variables: {
          recipeId,
          description: 'test_3',
          instructionType: 'MEAL_PREP',
          ordering: 1,
        },
      });

    // Expect that another user cannot add an instruction, and receive permission denied.
    expect(
      authenticated1Body.data.createRecipeInstruction.recipeInstruction
    ).toEqual({
      recipeInstructionId: expect.anything(),
      description: 'test_1',
      instructionType: 'MEAL_PREP',
      ordering: 0,
    });

    // Expect that another user cannot add an instruction, and receive permission denied.
    expect(authenticated2Body.errors[0].errcode).toEqual('42501');

    // Expect that unauthenticated users receive permission denied.
    expect(unauthenticatedBody.errors[0].errcode).toEqual('42501');
  });

  it('allows authenticated users to read their recipe instructions', async () => {
    const recipeName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: recipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName: recipeName,
          recipeType: 'MEAL',
        },
      });

    const { recipeId } = recipeBody.data.createRecipe.recipe;

    const { body: instructionBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeInstructionMutation,
        variables: {
          recipeId,
          description: 'test_1',
          instructionType: 'MEAL_PREP',
          ordering: 0,
        },
      });

    const {
      recipeInstructionId,
    } = instructionBody.data.createRecipeInstruction.recipeInstruction;

    const { body: authenticated1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: recipeInstructionQuery,
        variables: {
          recipeInstructionId,
        },
      });

    const { body: authenticated2Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: recipeInstructionQuery,
        variables: {
          recipeInstructionId,
        },
      });

    const { body: unauthenticatedBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: recipeInstructionQuery,
        variables: {
          recipeInstructionId,
        },
      });

    // Expect to read the recipe from the API.
    expect(authenticated1Body.data.recipeInstruction).toEqual({
      recipeInstructionId: expect.anything(),
      description: 'test_1',
      instructionType: 'MEAL_PREP',
      ordering: 0,
    });

    // Expect that requesting a recipe instruction from another user returns null.
    expect(authenticated2Body.data.recipeInstruction).toBeNull();

    // Expect that unauthenticated users cannot see the recipe instruction either.
    expect(unauthenticatedBody.data.recipeInstruction).toBeNull();
  });

  it('allows authenticated users to update recipe instructions', async () => {
    const recipeName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: recipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName: recipeName,
          recipeType: 'MEAL',
        },
      });

    const { recipeId } = recipeBody.data.createRecipe.recipe;

    const { body: authenticated1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeInstructionMutation,
        variables: {
          recipeId,
          description: 'test_1',
          instructionType: 'MEAL_PREP',
          ordering: 0,
        },
      });

    const { body: authenticated2Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: createRecipeInstructionMutation,
        variables: {
          recipeId,
          description: 'test_2',
          instructionType: 'MEAL_PREP',
          ordering: 1,
        },
      });

    const { body: unauthenticatedBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: createRecipeInstructionMutation,
        variables: {
          recipeId,
          description: 'test_3',
          instructionType: 'MEAL_PREP',
          ordering: 1,
        },
      });

    // Expect that another user cannot add an instruction, and receive permission denied.
    expect(
      authenticated1Body.data.createRecipeInstruction.recipeInstruction
    ).toEqual({
      recipeInstructionId: expect.anything(),
      description: 'test_1',
      instructionType: 'MEAL_PREP',
      ordering: 0,
    });

    // Expect that another user cannot add an instruction, and receive permission denied.
    expect(authenticated2Body.errors[0].errcode).toEqual('42501');

    // Expect that unauthenticated users receive permission denied.
    expect(unauthenticatedBody.errors[0].errcode).toEqual('42501');
  });

  it('allows authenticated users to delete recipe instructions', async () => {
    const recipeName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: recipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName: recipeName,
          recipeType: 'MEAL',
        },
      });

    const { recipeId } = recipeBody.data.createRecipe.recipe;

    const {
      body: {
        data: {
          createRecipeInstruction: {
            recipeInstruction: { recipeInstructionId: recipeInstructionId1 },
          },
        },
      },
    } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeInstructionMutation,
        variables: {
          recipeId,
          description: 'test_1',
          instructionType: 'MEAL_PREP',
          ordering: 0,
        },
      });

    const {
      body: {
        data: {
          createRecipeInstruction: {
            recipeInstruction: { recipeInstructionId: recipeInstructionId2 },
          },
        },
      },
    } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeInstructionMutation,
        variables: {
          recipeId,
          description: 'test_2',
          instructionType: 'MEAL_PREP',
          ordering: 1,
        },
      });

    const {
      body: {
        data: {
          createRecipeInstruction: {
            recipeInstruction: { recipeInstructionId: recipeInstructionId3 },
          },
        },
      },
    } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeInstructionMutation,
        variables: {
          recipeId,
          description: 'test_3',
          instructionType: 'MEAL_PREP',
          ordering: 2,
        },
      });

    const { body: authenticated1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: deleteRecipeInstructionMutation,
        variables: {
          recipeInstructionId: recipeInstructionId1,
        },
      });

    const { body: authenticated2Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: deleteRecipeInstructionMutation,
        variables: {
          recipeInstructionId: recipeInstructionId2,
        },
      });

    const { body: unauthenticatedBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: deleteRecipeInstructionMutation,
        variables: {
          recipeInstructionId: recipeInstructionId3,
        },
      });

    // Expect that another user cannot add an instruction, and receive permission denied.
    expect(
      authenticated1Body.data.deleteRecipeInstruction.recipeInstruction
    ).toEqual({
      recipeInstructionId: recipeInstructionId1,
    });

    // Expect that another user cannot add an instruction, and receive permission denied.
    expect(authenticated2Body.errors[0].message).toEqual(
      "No values were deleted in collection 'recipe_instructions' because no values you can delete were found matching these criteria."
    );

    // Expect that unauthenticated users receive permission denied.
    expect(unauthenticatedBody.errors[0].errcode).toEqual('42501');
  });
});
