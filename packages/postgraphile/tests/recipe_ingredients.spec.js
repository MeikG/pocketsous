const request = require('supertest');
const randomstring = require('randomstring');
const app = require('../app');
const { userIds, generateAccessToken } = require('./token');

const { GRAPHQL_ENDPOINT } = process.env;

const createIngredientMutation = `
  mutation($ingredientName: String!) {
    createIngredient(input: { ingredient: { ingredientName: $ingredientName } }) {
      ingredient {
        ingredientId
        ingredientName
      }
    }
  }
`;

const createRecipeMutation = `
  mutation($recipeName: String!, $recipeType: RecipeType!, $description: String) {
    createRecipe(
      input: {
        recipe: {
          recipeName: $recipeName
          recipeType: $recipeType
          description: $description
        }
      }
    ) {
      recipe {
        recipeId
        recipeName
      }
    }
  }
`;

const recipeQuery = `
  query($recipeId: UUID!) {
    recipe(recipeId: $recipeId) {
      recipeName
      ingredients {
        nodes {
          ingredient {
            ingredientId
          }
        }
      }
    }
  }
`;

const createRecipeIngredientMutation = `
  mutation(
    $recipeId: UUID!
    $ingredientId: UUID!
    $quantity: String!
    $ordering: Int!
  ) {
    createRecipeIngredient(
      input: {
        recipeIngredient: {
          recipeId: $recipeId
          ingredientId: $ingredientId
          quantity: $quantity
          ordering: $ordering
        }
      }
    ) {
      recipeIngredient {
        recipeId
        ingredientId
        quantity
        ordering
      }
    }
  }
`;

const updateRecipeIngredientMutation = `
  mutation(
    $recipeId: UUID!
    $ingredientId: UUID!
    $quantity: String
    $ordering: Int
  ) {
    updateRecipeIngredient(
      input: {
        recipeId: $recipeId
        ingredientId: $ingredientId
        patch: { quantity: $quantity, ordering: $ordering }
      }
    ) {
      recipeIngredient {
        quantity
        ordering
      }
    }
  }
`;

const deleteRecipeIngredientMutation = `
  mutation($recipeId: UUID!, $ingredientId: UUID!) {
    deleteRecipeIngredient(
      input: { recipeId: $recipeId, ingredientId: $ingredientId }
    ) {
      deletedRecipeIngredientNodeId
    }
  }
`;

describe('Recipe Ingredients', () => {
  it('rejects unauthenticated users from attaching or detaching recipe ingredients', async () => {
    const ingredientName = randomstring.generate();
    const recipeName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const { body: ingredientBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName: ingredientName,
        },
      });

    const { body: recipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName: recipeName,
          recipeType: 'MEAL',
        },
      });

    const { recipeId } = recipeBody.data.createRecipe.recipe;
    const { ingredientId } = ingredientBody.data.createIngredient.ingredient;

    const { body: attachmentBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: createRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
          quantity: 'test',
          ordering: 0,
        },
      });

    const { body: detachmentBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: deleteRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
        },
      });

    expect(attachmentBody.errors[0].errcode).toEqual('42501');
    expect(detachmentBody.errors[0].errcode).toEqual('42501');
  });

  it("prevents people attaching their ingredients to other people's recipes", async () => {
    const recipeName = randomstring.generate();
    const ingredientName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: recipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName,
          recipeType: 'MEAL',
        },
      });

    const { body: ingredientBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    const { recipeId } = recipeBody.data.createRecipe.recipe;
    const { ingredientId } = ingredientBody.data.createIngredient.ingredient;

    const { body: attachmentBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: createRecipeIngredientMutation,
        variables: {
          recipeId: recipeId,
          ingredientId: ingredientId,
          quantity: 'test',
          ordering: 0,
        },
      });

    const { body: detachmentBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: deleteRecipeIngredientMutation,
        variables: {
          recipeId: recipeId,
          ingredientId: ingredientId,
        },
      });

    expect(attachmentBody.errors[0].message).toEqual(
      'new row violates row-level security policy for table "recipe_ingredient"'
    );
    expect(detachmentBody.errors[0].message).toEqual(
      "No values were deleted in collection 'recipe_ingredients' because no values you can delete were found matching these criteria."
    );
  });

  it('attaches/detaches ingredients to recipes the user owns', async () => {
    const ingredientName = randomstring.generate();
    const recipeName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const { body: ingredientBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName: ingredientName,
        },
      });

    const { body: recipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName: recipeName,
          recipeType: 'MEAL',
        },
      });

    const { recipeId } = recipeBody.data.createRecipe.recipe;
    const { ingredientId } = ingredientBody.data.createIngredient.ingredient;

    const { body: attachmentBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
          quantity: 'test',
          ordering: 0,
        },
      });

    const { body: detachmentBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: deleteRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
        },
      });

    expect(
      attachmentBody.data.createRecipeIngredient.recipeIngredient
    ).not.toBeNull();

    expect(
      detachmentBody.data.deleteRecipeIngredient.recipeIngredient
    ).not.toBeNull();
  });

  it('prevents double attachment of an ingredient to a recipe', async () => {
    const ingredientName = randomstring.generate();
    const recipeName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const { body: ingredientBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName: ingredientName,
        },
      });

    const { body: recipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName: recipeName,
          recipeType: 'MEAL',
        },
      });

    const { recipeId } = recipeBody.data.createRecipe.recipe;
    const { ingredientId } = ingredientBody.data.createIngredient.ingredient;

    const { body: attachment1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
          quantity: 'test',
          ordering: 0,
        },
      });

    const { body: attachment2Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
          quantity: 'test',
          ordering: 0,
        },
      });

    const { body: detachmentBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: deleteRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
        },
      });

    expect(
      attachment1Body.data.createRecipeIngredient.recipeIngredient
    ).not.toBeNull();

    // Duplicate error code.
    expect(attachment2Body.errors[0].errcode).toEqual('23505');

    expect(
      detachmentBody.data.deleteRecipeIngredient.recipeIngredient
    ).not.toBeNull();
  });

  it('allows users to update the quantity and ordering of a recipe ingredient', async () => {
    const ingredientName = randomstring.generate();
    const recipeName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: ingredientBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName: ingredientName,
        },
      });

    const { body: recipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName: recipeName,
          recipeType: 'MEAL',
        },
      });

    const { recipeId } = recipeBody.data.createRecipe.recipe;
    const { ingredientId } = ingredientBody.data.createIngredient.ingredient;

    await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
          quantity: 'test',
          ordering: 0,
        },
      });

    const { body: updateAuthenticated1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: updateRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
          quantity: 'foobar',
          ordering: 1,
        },
      });

    const { body: updateAuthenticated2Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: updateRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
          quantity: 'foobar',
          ordering: 1,
        },
      });

    const { body: updateUnauthenticatedBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: updateRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
          quantity: 'foobar',
          ordering: 1,
        },
      });

    expect(
      updateAuthenticated1Body.data.updateRecipeIngredient.recipeIngredient
    ).toEqual({
      quantity: 'foobar',
      ordering: 1,
    });

    // User 2 cannot edit this recipe ingredient as they do not own it.
    expect(updateAuthenticated2Body.errors[0].message).toEqual(
      "No values were updated in collection 'recipe_ingredients' because no values you can update were found matching these criteria."
    );

    // Unauthenticated users should receive permission denied error code.
    expect(updateUnauthenticatedBody.errors[0].errcode).toEqual('42501');
  });

  it('allows users to delete recipe ingredients, but only if they own it', async () => {
    const ingredientName = randomstring.generate();
    const recipeName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: ingredientBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName: ingredientName,
        },
      });

    const { body: recipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName: recipeName,
          recipeType: 'MEAL',
        },
      });

    const { recipeId } = recipeBody.data.createRecipe.recipe;
    const { ingredientId } = ingredientBody.data.createIngredient.ingredient;

    await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
          quantity: 'test',
          ordering: 0,
        },
      });

    const { body: differentUserBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: deleteRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
        },
      });

    const { body: unauthenticatedBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: deleteRecipeIngredientMutation,
        variables: {
          recipeId,
          ingredientId,
        },
      });

    const { body: getRecipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: recipeQuery,
        variables: {
          recipeId,
        },
      });

    // Query the recipe to check that it has been successfully edited.
    expect(getRecipeBody.data.recipe.ingredients.nodes).toContainEqual({
      ingredient: {
        ingredientId,
      },
    });

    // User 2 cannot edit this recipe ingredient as they do not own it.
    expect(differentUserBody.errors[0].message).toEqual(
      "No values were deleted in collection 'recipe_ingredients' because no values you can delete were found matching these criteria."
    );

    // Unauthenticated users should receive permission denied error code.
    expect(unauthenticatedBody.errors[0].errcode).toEqual('42501');
  });
});
