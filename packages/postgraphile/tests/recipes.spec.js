const request = require('supertest');
const randomstring = require('randomstring');
const app = require('../app');
const { userIds, generateAccessToken } = require('./token');

const { GRAPHQL_ENDPOINT } = process.env;

const createRecipeMutation = `
  mutation($recipeName: String!, $recipeType: RecipeType!, $description: String) {
    createRecipe(
      input: {
        recipe: {
          recipeName: $recipeName
          recipeType: $recipeType
          description: $description
        }
      }
    ) {
      recipe {
        recipeId
        recipeName
      }
    }
  }
`;

const updateRecipeMutation = `
  mutation($recipeId: UUID!, $recipeName: String) {
    updateRecipe(input: {
      recipeId: $recipeId
      patch: {
        recipeName: $recipeName
      }
    }) {
      recipe {
        recipeId
        recipeName
      }
    }
  }
`;

const deleteRecipeMutation = `
  mutation($recipeId: UUID!) {
    deleteRecipe(input: { recipeId: $recipeId }) {
      recipe {
        recipeId
      }
    }
  }
`;

const recipeQuery = `
  query($recipeId: UUID!) {
    recipe(recipeId: $recipeId) {
      recipeName
    }
  }
`;

describe('Recipes', () => {
  it('rejects unauthenticated users from creating recipes', async () => {
    // Generate a random string for the new ingredient name.
    const recipeName = randomstring.generate();

    const { body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName,
          recipeType: 'MEAL',
        },
      });

    expect(body.errors[0].errcode).toEqual('42501');
  });

  it('creates new recipes that can only be seen by that individual user', async () => {
    // Generate a random string for the new ingredient name.
    const recipeName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });
    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: createRecipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName,
          recipeType: 'MEAL',
        },
      });

    const recipeId = createRecipeBody.data.createRecipe.recipe.recipeId;

    const { body: authenticated1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: recipeQuery,
        variables: {
          recipeId,
        },
      });

    const { body: authenticated2Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: recipeQuery,
        variables: {
          recipeId,
        },
      });

    const { body: unauthenticatedBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: recipeQuery,
        variables: {
          recipeId,
        },
      });

    // Expect that authenticated user 1 returns the recipe.
    expect(authenticated1Body.data.recipe.recipeName).toEqual(recipeName);

    // Expect that unauthenticated and other users return null.
    expect(authenticated2Body.data.recipe).toBeNull();
    expect(unauthenticatedBody.data.recipe).toBeNull();
  });

  it('allows only authenticated users to update their recipes', async () => {
    // Generate a random string for the new ingredient name.
    const recipeName = randomstring.generate();
    const newRecipe1Name = randomstring.generate();
    const newRecipe2Name = randomstring.generate();
    const newRecipe3Name = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });
    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: createRecipeBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName,
          recipeType: 'MEAL',
        },
      });

    const recipeId = createRecipeBody.data.createRecipe.recipe.recipeId;

    const { body: authenticated1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: updateRecipeMutation,
        variables: {
          recipeId,
          recipeName: newRecipe1Name,
        },
      });

    const { body: authenticated2Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: updateRecipeMutation,
        variables: {
          recipeId,
          recipeName: newRecipe2Name,
        },
      });

    const { body: unauthenticatedBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: updateRecipeMutation,
        variables: {
          recipeId,
          recipeName: newRecipe3Name,
        },
      });

    const { body: authenticatedFinalBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: recipeQuery,
        variables: {
          recipeId,
        },
      });

    // Expect that authenticated user 1 returns the recipe.
    expect(authenticated1Body.data.updateRecipe.recipe.recipeName).toEqual(
      newRecipe1Name
    );

    // Expect that unauthenticated and other users return null.
    expect(authenticated2Body.errors[0].message).toEqual(
      "No values were updated in collection 'recipes' because no values you can update were found matching these criteria."
    );

    // Expect permission denied.
    expect(unauthenticatedBody.errors[0].errcode).toEqual('42501');

    // Expect that, after two attempted updates, the name has not been modified.
    expect(authenticatedFinalBody.data.recipe.recipeName).toEqual(
      newRecipe1Name
    );
  });

  it('allows only authenticated users to delete their recipes', async () => {
    const recipe1Name = randomstring.generate();
    const recipe2Name = randomstring.generate();
    const recipe3Name = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });
    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: createRecipe1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName: recipe1Name,
          recipeType: 'MEAL',
        },
      });

    const { body: createRecipe2Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName: recipe2Name,
          recipeType: 'MEAL',
        },
      });

    const { body: createRecipe3Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createRecipeMutation,
        variables: {
          recipeName: recipe3Name,
          recipeType: 'MEAL',
        },
      });

    const recipe1Id = createRecipe1Body.data.createRecipe.recipe.recipeId;
    const recipe2Id = createRecipe2Body.data.createRecipe.recipe.recipeId;
    const recipe3Id = createRecipe3Body.data.createRecipe.recipe.recipeId;

    const { body: authenticated1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: deleteRecipeMutation,
        variables: {
          recipeId: recipe1Id,
        },
      });

    const { body: authenticated2Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: deleteRecipeMutation,
        variables: {
          recipeId: recipe2Id,
        },
      });

    const { body: unauthenticatedBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: deleteRecipeMutation,
        variables: {
          recipeId: recipe3Id,
        },
      });

    // Expect that authenticated user 1 returns the recipe.
    expect(authenticated1Body.data.deleteRecipe.recipe.recipeId).toEqual(
      recipe1Id
    );

    // Expect that unauthenticated and other users return null.
    expect(authenticated2Body.errors[0].message).toEqual(
      "No values were deleted in collection 'recipes' because no values you can delete were found matching these criteria."
    );

    // Expect permission denied.
    expect(unauthenticatedBody.errors[0].errcode).toEqual('42501');
  });
});
