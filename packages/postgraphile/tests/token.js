const jwt = require('jsonwebtoken');

const { AUTHENTICATION_SECRET, AUTHENTICATION_AUDIENCE } = process.env;

const userIds = {
  authenticated1: 'aed390b2-c6d3-4d9b-87e9-787d3327e7de',
  authenticated2: 'f7979faf-3aed-4813-917b-11205874e6fc',
  authenticated3: '083915c5-f23f-47d2-abf9-b0af707afc8d',
  authenticated4: 'b5463bd5-d066-493a-9672-071936a7ed68',
  admin1: 'b5463bd5-d066-493a-9672-071936a7ed68',
  admin2: 'c4bfe40c-d46b-4a69-9c8a-38abb5adbf9c',
};

function generateAccessToken(payload, options, secret = AUTHENTICATION_SECRET) {
  const defaultPayload = {
    role: 'authenticated',
    method: 'google',
  };

  const defaultOptions = {
    algorithm: 'HS512',
    issuer: AUTHENTICATION_AUDIENCE,
    audience: AUTHENTICATION_AUDIENCE,
  };

  return jwt.sign({ ...defaultPayload, ...payload }, secret, {
    ...defaultOptions,
    ...options,
  });
}

module.exports = {
  userIds,
  generateAccessToken,
};
