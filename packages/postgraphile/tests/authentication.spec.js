const request = require('supertest');
const app = require('../app');
const { userIds, generateAccessToken } = require('./token');

const { GRAPHQL_ENDPOINT } = process.env;

const getCurrentUserQuery = `
  {
    getCurrentUser {
      displayName
    }
  }
`;

const getCurrentRoleQuery = `
  {
    getCurrentUser {
      role
    }
  }
`;

describe('Authentication', () => {
  it('returns when requesting getCurrentUser for unauthenticated users', async () => {
    const response = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: getCurrentUserQuery,
      });

    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual({
      data: {
        getCurrentUser: null,
      },
    });
  });

  it('returns the correct name when authenticated', async () => {
    const token = generateAccessToken({ sub: userIds.authenticated1 });
    const response = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)
      .send({
        query: getCurrentUserQuery,
      });

    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual({
      data: {
        getCurrentUser: {
          displayName: 'test_authenticated_1',
        },
      },
    });
  });

  it('fails silently if a user_id references a non-existent user', async () => {
    const token = generateAccessToken({
      sub: 'ad65cfd5-98d1-4acd-a265-7af2277cdd3a',
    });
    const response = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)
      .send({
        query: getCurrentUserQuery,
      });

    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual({
      data: {
        getCurrentUser: null,
      },
    });
  });

  it('fails if the JWT is passed in incorrectly', async () => {
    const token = generateAccessToken({
      sub: 'ad65cfd5-98d1-4acd-a265-7af2277cdd3a',
    });
    const response = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `${token}`)
      .send({
        query: getCurrentUserQuery,
      });

    expect(response.statusCode).toBe(400);
    expect(response.body).toEqual({
      errors: [
        {
          message:
            'Authorization header is not of the correct bearer scheme format.',
        },
      ],
    });
  });

  it('fails if the JWT is expired', async () => {
    const token = generateAccessToken({
      sub: userIds.authenticated1,
      iat: 0,
      exp: 100,
    });

    const { statusCode, body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)
      .send({
        query: getCurrentUserQuery,
      });

    expect(statusCode).toBe(401);
    expect(body).toEqual({
      errors: [
        {
          message: 'jwt expired',
        },
      ],
    });
  });

  it('fails if the JWT is signed incorrectly', async () => {
    const token = generateAccessToken(
      {
        sub: userIds.authenticated1,
      },
      {},
      'foobar'
    );

    const { statusCode, body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)
      .send({
        query: getCurrentUserQuery,
      });

    expect(statusCode).toBe(403);
    expect(body).toEqual({
      errors: [
        {
          message: 'invalid signature',
        },
      ],
    });
  });

  it('rejects JWTs with an algorithm: none exploit attempt', async () => {
    const token = generateAccessToken(
      {
        sub: userIds.authenticated1,
      },
      { algorithm: 'none' }
    );

    const { statusCode, body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)
      .send({
        query: getCurrentUserQuery,
      });

    expect(statusCode).toBe(403);
    expect(body).toEqual({
      errors: [
        {
          message: 'jwt signature is required',
        },
      ],
    });
  });

  it('correctly returns the role of the user (authenticated)', async () => {
    const token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const { statusCode, body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)
      .send({
        query: getCurrentRoleQuery,
      });

    expect(statusCode).toBe(200);
    expect(body).toEqual({
      data: {
        getCurrentUser: {
          role: 'AUTHENTICATED',
        },
      },
    });
  });

  it('correctly returns the role of the user (admin)', async () => {
    const token = generateAccessToken({
      sub: userIds.admin1,
    });

    const { statusCode, body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)
      .send({
        query: getCurrentRoleQuery,
      });

    expect(statusCode).toBe(200);
    expect(body).toEqual({
      data: {
        getCurrentUser: {
          role: 'ADMIN',
        },
      },
    });
  });
});
