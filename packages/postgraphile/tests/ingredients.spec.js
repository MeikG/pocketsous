const request = require('supertest');
const randomstring = require('randomstring');
const app = require('../app');
const { userIds, generateAccessToken } = require('./token');

const { GRAPHQL_ENDPOINT } = process.env;

const allIngredientsQuery = `
  {
    ingredients {
      nodes {
        ingredientName
      }
    }
  }
`;

const createIngredientMutation = `
  mutation($ingredientName: String!) {
    createIngredient(input: { ingredient: { ingredientName: $ingredientName } }) {
      ingredient {
        ingredientId
        ingredientName
      }
    }
  }
`;

const updateIngredientMutation = `
  mutation($ingredientId: UUID!, $ingredientName: String!) {
    updateIngredient(
      input: {
        ingredientId: $ingredientId
        patch: { ingredientName: $ingredientName }
      }
    ) {
      ingredient {
        ingredientName
      }
    }
  }
`;

const deleteIngredientMutation = `
  mutation($ingredientId: UUID!) {
    deleteIngredient(input: { ingredientId: $ingredientId }) {
      ingredient {
        ingredientId
      }
    }
  }
`;

const autocompleteIngredientsQuery = `
  query ($text: String!){
    autocompleteIngredients(text: $text) {
      nodes {
        ingredientName
      }
    }
  }
`;

describe('Ingredients', () => {
  it('rejects unauthenticated users from creating ingredients', async () => {
    // Generate a random string for the new ingredient name.
    const ingredientName = randomstring.generate();

    const { body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    expect(body.errors[0].errcode).toEqual('42501');
  });

  it('creates new ingredients that can only be seen by that individual user', async () => {
    // Generate a random string for the new ingredient name.
    const ingredientName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });
    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    const { body: authenticated1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: allIngredientsQuery,
      });

    const { body: authenticated2Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: allIngredientsQuery,
      });

    const { body: unauthenticatedBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: allIngredientsQuery,
      });

    expect(authenticated1Body.data.ingredients.nodes).toContainEqual({
      ingredientName,
    });

    expect(authenticated2Body.data.ingredients.nodes).not.toContainEqual({
      ingredientName,
    });

    expect(unauthenticatedBody.data.ingredients.nodes).not.toContainEqual({
      ingredientName,
    });
  });

  it('prevents users from creating ingredients with the same name', async () => {
    // Generate a random string for the new ingredient name.
    const ingredientName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    const { body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    // Duplicate error code.
    expect(body.errors[0].errcode).toEqual('23505');
  });

  it("doesn't prevent multiple users from having the same ingredient", async () => {
    // Generate a random string for the new ingredient name.
    const ingredientName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: authenticated1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    const { body: authenticated2Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    // Expect that the ingredient name is the same as the one created.
    expect(
      authenticated1Body.data.createIngredient.ingredient.ingredientName
    ).toEqual(ingredientName);

    expect(
      authenticated2Body.data.createIngredient.ingredient.ingredientName
    ).toEqual(ingredientName);

    // Expect that the ingredientId of auth1 is different to auth2.
    expect(
      authenticated1Body.data.createIngredient.ingredient.ingredientId
    ).not.toEqual(
      authenticated2Body.data.createIngredient.ingredient.ingredientId
    );
  });

  it('prevents unauthenticated users from updating user ingredients', async () => {
    // Generate a random string for the new ingredient name.
    const ingredientName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const { body: authenticated1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    const { body: unauthenticatedBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: updateIngredientMutation,
        variables: {
          ingredientId:
            authenticated1Body.data.createIngredient.ingredient.ingredientId,
          ingredientName: 'foobar',
        },
      });

    // Expect that the ingredient name is the same as the one created.
    expect(unauthenticatedBody.errors[0].errcode).toEqual('42501');
  });

  it("prevents authenticated users from updating another user's ingredients", async () => {
    // Generate a random string for the new ingredient name.
    const ingredientName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: authenticated1Body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    const { body: unauthenticatedBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: updateIngredientMutation,
        variables: {
          ingredientId:
            authenticated1Body.data.createIngredient.ingredient.ingredientId,
          ingredientName: 'foobar',
        },
      });

    // Auth 2 attempting to update this ingredient should get an "ingredient not found" error.
    expect(unauthenticatedBody.errors[0].message).toEqual(
      "No values were updated in collection 'ingredients' because no values you can update were found matching these criteria."
    );
  });

  it('allows an authenticated user to update their own ingredient', async () => {
    // Generate a random string for the new ingredient name.
    const ingredientName = randomstring.generate();
    const newIngredientName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const { body: createIngredientBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    const { body: updateIngredientBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: updateIngredientMutation,
        variables: {
          ingredientId:
            createIngredientBody.data.createIngredient.ingredient.ingredientId,
          ingredientName: newIngredientName,
        },
      });

    // Expect that the ingredient name is the same as the one created.
    expect(
      updateIngredientBody.data.updateIngredient.ingredient.ingredientName
    ).toEqual(newIngredientName);
  });

  it('prevents an authenticated user from updating a public ingredient', async () => {
    const ingredientId = '664d5425-406c-4d74-94f7-c052501d8539';
    const ingredientName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const { body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: updateIngredientMutation,
        variables: {
          ingredientId,
          ingredientName,
        },
      });

    // Expect that the user was prevented from updating the public ingredient.
    expect(body.errors[0].message).toEqual(
      "No values were updated in collection 'ingredients' because no values you can update were found matching these criteria."
    );
  });

  it('prevents an unauthenticated user from deleting a public ingredient', async () => {
    // Ingredient ID of public ingredient.
    const ingredientId = '664d5425-406c-4d74-94f7-c052501d8539';

    const { body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        query: deleteIngredientMutation,
        variables: {
          ingredientId,
        },
      });

    // Expect permission denied error code.
    expect(body.errors[0].errcode).toEqual('42501');
  });

  it('prevents an authenticated user from deleting a public ingredient', async () => {
    // Ingredient ID of public ingredient.
    const ingredientId = '664d5425-406c-4d74-94f7-c052501d8539';
    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const { body } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: deleteIngredientMutation,
        variables: {
          ingredientId,
        },
      });

    // Expect permission denied error code.
    expect(body.errors[0].message).toEqual(
      "No values were deleted in collection 'ingredients' because no values you can delete were found matching these criteria."
    );
  });

  it('allows a user to delete their own recipe', async () => {
    // Ingredient ID of public ingredient.
    const ingredientName = randomstring.generate();
    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const { body: createIngredientBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    const { body: deleteIngredientBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: deleteIngredientMutation,
        variables: {
          ingredientId:
            createIngredientBody.data.createIngredient.ingredient.ingredientId,
        },
      });

    // Expect that the ingredient has been successfully deleted.
    expect(
      deleteIngredientBody.data.deleteIngredient.ingredient.ingredientId
    ).toEqual(
      createIngredientBody.data.createIngredient.ingredient.ingredientId
    );
  });

  it("prevents an authenticated user from deleting another user's ingredient", async () => {
    const ingredientName = randomstring.generate();

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    const authenticated2Token = generateAccessToken({
      sub: userIds.authenticated2,
    });

    const { body: createIngredientBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    const { body: deleteIngredientBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated2Token}`)
      .send({
        query: deleteIngredientMutation,
        variables: {
          ingredientId:
            createIngredientBody.data.createIngredient.ingredient.ingredientId,
        },
      });

    expect(deleteIngredientBody.errors[0].message).toEqual(
      "No values were deleted in collection 'ingredients' because no values you can delete were found matching these criteria."
    );
  });

  it('autocompletes ingredients', async () => {
    // Generate an ingredient name out of a series of random words.
    const ingredient1Word = randomstring.generate(3);
    const ingredient2Word = randomstring.generate(5);
    const ingredient3Word = randomstring.generate(3);

    const ingredientName = [
      ingredient1Word,
      ingredient2Word,
      ingredient3Word,
    ].join(' ');

    const authenticated1Token = generateAccessToken({
      sub: userIds.authenticated1,
    });

    await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: createIngredientMutation,
        variables: {
          ingredientName,
        },
      });

    const { body: ingredient1WordBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: autocompleteIngredientsQuery,
        variables: {
          text: ingredient1Word,
        },
      });

    const { body: ingredient2WordBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: autocompleteIngredientsQuery,
        variables: {
          text: ingredient2Word,
        },
      });

    const { body: ingredient3WordBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: autocompleteIngredientsQuery,
        variables: {
          text: ingredient3Word,
        },
      });

    const { body: ingredient2PartialBody } = await request(app)
      .post(GRAPHQL_ENDPOINT)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authenticated1Token}`)
      .send({
        query: autocompleteIngredientsQuery,
        variables: {
          // Only search for the first two letters of the ingredient.
          text: ingredient2Word.substr(0, 2),
        },
      });

    // Expect that, by searching for each word, it returns the ingredient.
    expect(
      ingredient1WordBody.data.autocompleteIngredients.nodes
    ).toContainEqual({ ingredientName });

    expect(
      ingredient2WordBody.data.autocompleteIngredients.nodes
    ).toContainEqual({ ingredientName });

    expect(
      ingredient3WordBody.data.autocompleteIngredients.nodes
    ).toContainEqual({ ingredientName });

    // Expect that a partial word will match the ingredient too.
    expect(
      ingredient2PartialBody.data.autocompleteIngredients.nodes
    ).toContainEqual({ ingredientName });
  });
});
