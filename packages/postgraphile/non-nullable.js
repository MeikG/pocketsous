module.exports = function NonNullRelationsPlugin(builder) {
  builder.hook('GraphQLObjectType:fields:field', (field, build, context) => {
    const {
      scope: { isPgForwardRelationField, isPgBackwardSingleRelationField },
    } = context;

    if (isPgForwardRelationField) {
      return {
        ...field,
        type: new build.graphql.GraphQLNonNull(field.type),
      };
    }
    return field;
  });
};
