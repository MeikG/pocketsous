const { makeChangeNullabilityPlugin } = require('graphile-utils');

module.exports = makeChangeNullabilityPlugin({
  Ingredient: {
    user: true,
  },
  Image: {
    original: false,
    portraitCard: false,
    titleCard: false,
    thumbnail: false,
  },
  Colour: {
    red: false,
    green: false,
    blue: false,
  },
  autocompleteIngredients: {
    searchText: false,
  },
});
