const {
  NODE_ENV,
  AUTHENTICATION_SECRET,
  AUTHENTICATION_AUDIENCE,
  GRAPHQL_ENDPOINT,
  GRAPHIQL_ENDPOINT,
} = process.env;

const dev = NODE_ENV === 'development';

const configuration = {
  ignoreRBAC: false,
  graphiql: dev,
  enhanceGraphiql: dev,
  allowExplain: dev,
  watchPg: dev,
  retryOnInitFail: true,
  pgDefaultRole: 'anonymous',
  jwtPgTypeIdentifier: 'public.jwt_token',
  jwtSecret: AUTHENTICATION_SECRET,
  jwtVerifyOptions: {
    algorithms: ['HS512'],
    issuer: AUTHENTICATION_AUDIENCE,
    audience: AUTHENTICATION_AUDIENCE,
    maxAge: 3700,
  },
  enableCors: true,
  appendPlugins: [
    require('./non-nullable'),
    require('./change-nullability'),
    require('./version'),
    require('@graphile-contrib/pg-simplify-inflector'),
  ],
  showErrorStack: dev,
  extendedErrors: dev ? ['hint', 'detail', 'errcode'] : ['errcode'],
  dynamicJson: true,
  subscriptions: true,
  graphileBuildOptions: {
    connectionFilterAllowedOperators: [
      'equalTo',
      'lessThan',
      'lessThanOrEqualTo',
      'greaterThan',
      'greaterThanOrEqualTo',
      'in',
    ],
    connectionFilterAllowedFieldTypes: ['Date', 'Time'],
    connectionFilterComputedColumns: false,
    connectionFilterLists: false,
    connectionFilterLogicalOperators: false,
    nestedMutationsSimpleFieldNames: true,
    nestedMutationsDeleteOthers: false,
  },
  setofFunctionsContainNulls: false,
  ignoreIndexes: false,
  enableQueryBatching: true,
  legacyRelations: 'omit',
  graphqlRoute: GRAPHQL_ENDPOINT,
  graphiqlRoute: GRAPHIQL_ENDPOINT,
};

module.exports = configuration;
