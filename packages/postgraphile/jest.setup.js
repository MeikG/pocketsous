const POSTGRES_USER = 'root';
const POSTGRES_PASSWORD = 'pwd';
const POSTGRES_DB = 'test';

process.env.GRAPHQL_ENDPOINT = '/graphql';
process.env.NODE_ENV = 'test';
process.env.POSTGRES_USER = POSTGRES_USER;
process.env.POSTGRES_PASSWORD = POSTGRES_PASSWORD;
process.env.POSTGRES_SERVER = 'localhost';
process.env.POSTGRES_DB = POSTGRES_DB;
process.env.AUTHENTICATION_SECRET = 'test';
process.env.AUTHENTICATION_AUDIENCE = 'test';

const { postgraphile } = require('postgraphile');
const { GenericContainer, Wait } = require('testcontainers');
const path = require('path');
const app = require('./app');
const configuration = require('./configuration');

const { CI } = process.env;

// Module-scoped variables for storing the containers for teardown later.
let postgresContainer;
let flywayContainer;

beforeAll(async done => {
  // Spin up a new postgres container.
  postgresContainer = await new GenericContainer('postgres', '12-alpine')
    .withEnv('POSTGRES_USER', POSTGRES_USER)
    .withEnv('POSTGRES_PASSWORD', POSTGRES_PASSWORD)
    .withEnv('POSTGRES_DB', POSTGRES_DB)
    .withBindMount(path.join(__dirname, '__fixtures__'), '/sql')
    .withExposedPorts(5432)
    .start();

  // Get the host postgres port.
  const postgresPort = postgresContainer.getMappedPort(5432);

  const {
    isReadyOutput,
    exitCode: isReadyExitCode,
  } = await postgresContainer.exec(['pg_isready']);
  if (isReadyExitCode !== 0) {
    console.error(isReadyOutput);
    throw new Error('Database failed to start properly.');
  }

  // Spin up a flyway container, and execute the migration process.
  flywayContainer = await new GenericContainer('flyway/flyway', '6.1')
    .withEnv('FLYWAY_URL', `jdbc:postgresql://localhost:${postgresPort}/test`)
    .withEnv('FLYWAY_USER', POSTGRES_USER)
    .withEnv('FLYWAY_PASSWORD', POSTGRES_PASSWORD)
    .withEnv('FLYWAY_PLACEHOLDERS_IMAGE_UPLOAD_USER', 'image_upload')
    .withEnv(
      'FLYWAY_PLACEHOLDERS_IMAGE_UPLOAD_PASSWORD',
      'image_upload_password'
    )
    .withEnv('FLYWAY_PLACEHOLDERS_IMAGE_UPLOAD_ORIGINAL_FILENAME', 'original')
    .withEnv(
      'FLYWAY_PLACEHOLDERS_IMAGE_UPLOAD_TITLE_CARD_FILENAME',
      'title_card'
    )
    .withEnv(
      'FLYWAY_PLACEHOLDERS_IMAGE_UPLOAD_PORTRAIT_CARD_FILENAME',
      'portrait_card'
    )
    .withEnv('FLYWAY_PLACEHOLDERS_IMAGE_UPLOAD_THUMBNAIL_FILENAME', 'thumbnail')
    .withEnv(
      'FLYWAY_PLACEHOLDERS_GOOGLE_AUTHENTICATION_USER',
      'google_authentication'
    )
    .withEnv(
      'FLYWAY_PLACEHOLDERS_GOOGLE_AUTHENTICATION_PASSWORD',
      'google_authentication_password'
    )
    .withBindMount(
      path.join(__dirname, '../', 'flyway', 'migrations'),
      '/flyway/sql'
    )
    .withNetworkMode('host')
    .withCmd(['migrate'])
    // Wait for the migration to finish.
    .withWaitStrategy(Wait.forLogMessage('Successfully applied'))
    .start();

  await flywayContainer.stop();

  const {
    output: seedOutput,
    exitCode: seedExitCode,
  } = await postgresContainer.exec([
    'psql',
    '--username',
    POSTGRES_USER,
    '--dbname',
    POSTGRES_DB,
    '-a',
    '-f',
    '/sql/init.sql',
  ]);
  if (seedExitCode !== 0) {
    console.error(seedOutput);
    throw new Error('Database failed to seed properly');
  }

  // Determine where the Postgres database is located.
  const host = !!CI ? 'docker' : 'localhost';

  // Start Postgraphile and wait for the schema to build.
  app.use(
    postgraphile(
      `postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${host}:${postgresPort}/${POSTGRES_DB}`,
      'public',
      configuration
    )
  );

  done();
});

afterAll(async () => {
  // Clean-up the postgres container.
  await postgresContainer.stop();
});
