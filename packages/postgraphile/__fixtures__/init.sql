INSERT INTO public.registered_user(user_id, photo, display_name, role) VALUES
    ('aed390b2-c6d3-4d9b-87e9-787d3327e7de', 'test_authenticated_1', 'test_authenticated_1', 'authenticated'),
    ('f7979faf-3aed-4813-917b-11205874e6fc', 'test_authenticated_2', 'test_authenticated_2', 'authenticated'),
    ('083915c5-f23f-47d2-abf9-b0af707afc8d', 'test_authenticated_3', 'test_authenticated_3', 'authenticated'),
    ('5689b202-be89-47be-862a-8d3c3a816edb', 'test_authenticated_4', 'test_authenticated_4', 'authenticated'),
    ('b5463bd5-d066-493a-9672-071936a7ed68', 'test_admin_1', 'test_admin_1', 'admin'),
    ('c4bfe40c-d46b-4a69-9c8a-38abb5adbf9c', 'test_admin_2', 'test_admin_2', 'admin');

INSERT INTO public.user_preference(user_id, first_time, marketing, marketing_accepted) VALUES
    ('aed390b2-c6d3-4d9b-87e9-787d3327e7de', true, false, null),
    ('f7979faf-3aed-4813-917b-11205874e6fc', false, false, null),
    ('083915c5-f23f-47d2-abf9-b0af707afc8d', true, true, null),
    ('5689b202-be89-47be-862a-8d3c3a816edb', false, true, null),
    ('b5463bd5-d066-493a-9672-071936a7ed68', false, false, null),
    ('c4bfe40c-d46b-4a69-9c8a-38abb5adbf9c', false, true, null);


